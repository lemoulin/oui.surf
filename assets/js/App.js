import '!!script!jquery-migrate/jquery-migrate.min';
import appLoading from 'app-loading';
import inViewport from 'in-viewport';
import domready from 'domready';
import { Swiper } from 'swiper';


// local files
import components from './components';
import modules from './modules';
import controllers from './controllers';
import DOMRouter from './core/DOMRouter';

// locale modules
import FastOnScroll from './modules/FastOnScroll';
import { mobileDetect } from './core/Mobile';

// Defines the router
let router = new DOMRouter(controllers);

// constants
const colorGold = "#9F8C48";
const AppContainer = '#document';

// start app
domready(()=> {
	new App();
});

// show transport bar on page change
// $( window ).on('onbeforeunload', function() {
// 	console.log("window unload");
// 	appLoading.setColor(colorGold);
// 	appLoading.start();
// });

/*
* Main app
*/
class App {

	constructor() {
		this.init();
	}

	init() {
		// init the DOM router
		router.init();

		// check every 0.5 seconds for infinite scroll element to trigger
		new FastOnScroll(500, this.infiniteScroll);

		// add touch device class on body
		if ( mobileDetect.mobile() ) {
			$("body").addClass('touch-device').removeClass('desktop-device');
		}

    // init facebook like buttons
    setTimeout(function () {
        deferFB()
    }, 2000);

		// Easter egg
		var args = [
      '\n %c %c %c Bienvenue chez Oui.Surf! 🏄  %c ' + ' %c ' + ' info@oui.surf  %c %c ✈ 🌊 🏄 \n\n',
      'background: #b39e53; padding:5px 0;',
      'background: #b39e53; padding:5px 0;',
      'color: #ffffff; background: #9F8C48; padding:5px 0;',
      'background: #b39e53; padding:5px 0;',
      'background: #988543; color: #ffffff; padding:5px 0;',
      'background: #b39e53; padding:5px 5px;',
      'color: #ff2424; background: #fff; padding:5px 0;'
    ];
    window.console.log.apply(console, args);
	}

	infiniteScroll(initCallback) {
		let offset    = $(window).height() * 1.5;

		// get first infinite scroll element in page
		let element   = $('[data-infinite-scroll-nav] > a').filter(":first").get(0);

		// check if visible
		let isVisible = (element) ? inViewport(element, { "offset": offset }) : false;

		// modules & component after page load
		let _DOMInit = function() {
			components.Figures.init();
			components.Sliders.init();
			components.Nav.init();
			modules.Players.init();
			router.init();

			// parse Facebook like buttons
			try {
					console.log('reparse like buttons');
					FB.XFBML.parse();
			} catch (e) {
					console.error(e);
			}

			// GetSocial reset/reload
			// if (GSLoader.reset() !== undefined) GSLoader.reset();
		};


		if (isVisible) {
			// get url
			let url = element.getAttribute("href");

			// detach element from page
			element.remove();

			// start loading bar
			appLoading.setColor(colorGold);
			appLoading.start();

			// XHR get next post
			$.get(url, (response) => {
				// title
				let title = $("<span/>").html(response).find('title').filter(":first").text();

				// change url and document title
				// window.history.replaceState({}, title, url);
				// document.title = title;

				// get current post
				let $currentPost = $('main > article.post').filter(":last");

				// insert new post next to it
				let $nextPost = $(response).find('main > article.post').eq(0).insertAfter(  $currentPost );

				// send page view to GA
				try {
					ga('send', 'pageview', {
						'page': window.location.pathname
					});
				} catch (e) {
					console.warn(e, "No tracking installed");
				}

				// re init DOM
				_DOMInit();

				// stop loader
				appLoading.stop();
			});

		}

        // check for current URL to set based on visible data-permalink element in page
        let articles = document.querySelectorAll('[data-permalink]');
        _.forEach(articles, (e) => {
            // check if is in viewport
            if ( inViewport( e ) ) {

                // console.log("in viewport, break loop, change URL for current");

                let title = $(e).find('.entry-title').filter(":first").text();
                let url   = $(e).data('permalink');

                // change URL if not the same
                if(url != document.location.href) {
                    window.history.replaceState({}, title, url);
    				document.title = title;
                }

                // stop foreach
                return false;
            }
        });
	}


}
