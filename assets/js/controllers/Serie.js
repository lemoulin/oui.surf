import EpisodesSlider from './Series';
import { setHeaderTransparent } from "../components/Header";

/*
* Serie detail page
*/

export default {

	init() {
		// set header transparent
		setHeaderTransparent();
	}

};
