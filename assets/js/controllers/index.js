import Home from './Home';
import SectionSeries from './SectionSeries';
import SectionVideos from './SectionVideos';
import Serie from './Serie';
import Post from './Post';

// components index
const controllers = {
	'Home': Home,
	'SectionSeries': SectionSeries,
	'SectionVideos': SectionVideos,
	'Serie': Serie,
	'Post': Post
};

export default controllers;
