import { setHeaderTransparent } from "../components/Header";
import { parseVideoBackgrounds } from "../components/Figures";
import { mobileDetect } from '../core/Mobile';

/*
* Get post current section (videos, article, dossier)
*/
export function getPostSection() {
    return document.querySelector('[data-ouisurf-section]').getAttribute('data-ouisurf-section');
}

/*
* Post page
*/
export default {

    /*
    * Articles section post init
    */
    articles() {
        this.init();
    },

    /*
    * Guides post init
    */
    guides() {
        this.init();
    },

    /*
    * Dossiers post init
    */
    dossiers() {
        this.init();
    },

    /*
    * Serie post init
    */
    series() {
        this.init();
    },

    /*
    * Video post init
    */
    videos() {
        this.init();
    },

    /*
    * Parse content in post
    */
    parseContent() {
        /*
        * Video backgrounds
        * parse mp4 files from <a> elements and transform to video background
        */

        let videoBackgroundElements = $('[class^=entry-content]').find('a[href$=mp4]');
        videoBackgroundElements.each(function() {
            // get filename
            let filename  = $(this).attr('href');

            // create div container, append next to <a>
            let $div = $("<div/>").attr('data-video-bg', filename).insertAfter( this );

            // remove original <a> element
            this.remove();
        });

        // then parse all created [video-bg] elements
        parseVideoBackgrounds();
    },

    /*
    * standar init for post
    */
    init() {
        // find header
        let $header = $("[class^=post-header]");

        // if header has image or video, set header transparent
        if ( mobileDetect.phone() ) {

            if ( $header.hasClass('with-background') && !$header.hasClass('with-embeded-video') ) {
                setHeaderTransparent();
            }

        } else {

            if ( $header.hasClass('with-background') ) {
                setHeaderTransparent();
            }

        }

        // parse post content
        this.parseContent();
    }


};
