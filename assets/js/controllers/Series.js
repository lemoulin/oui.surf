import { gridGutter } from '../core/Settings';
import { bLazyInstance } from '../components/Figures';

const gridTotalCols = 12;

/*
* Serie detail page
*/
class EpisodesSlider {

	constructor(selector = '.serie--episodes-slider', slidesPerView = 1.75, offset = 0, offsetWithGutter = true) {
		this.selector = selector;
		this.slidesPerView = slidesPerView;
		this.offset = offset;
		this.gutter = offsetWithGutter ? gridGutter / 2 : 0;

		// init class
		this.init();
	}

	init() {
		let e = document.querySelector(this.selector);
		let swiper = new Swiper(e, {
			freeMode: true,
			prevButton: e.querySelector('.slider--arrow--prev'),
			nextButton: e.querySelector('.slider--arrow--next'),
			slidesPerView: this.slidesPerView,
			spaceBetween: gridGutter,
			slidesOffsetAfter: gridGutter
			// slidesOffsetBefore: this.calculateLeftOffset()
		});

		// revalidation image lazy loading on each slider transition start
		swiper.on('onTransitionStart', (swiper) => {
			bLazyInstance.revalidate();
		});

	}

	calculateLeftOffset() {
		let colWidth = $(window).width() / gridTotalCols;
		return (colWidth + this.gutter) * this.offset;
	}


}

/*
* Create a slider of each episodes list
*/
export default EpisodesSlider;
