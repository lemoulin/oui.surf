import EpisodesSlider from './Series';
import { setHeaderTransparent } from "../components/Header";
import { mobileDetect } from '../core/Mobile';

/*
* Section:videos landing page
*/

export default {

	init() {
		// set header transaprent, except mobile
		if( !mobileDetect.mobile() ) setHeaderTransparent();
	}



};
