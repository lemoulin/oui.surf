import { setHeaderTransparent } from "../components/Header";

/*
* Landing page
*/
export default {

  /*
  * standar init for post
  */
  init() {
    let fullscreenSlider = $('.post-slider.is-fullscreen').get(0);
    if (fullscreenSlider) {
      setHeaderTransparent();
    }
  }


};
