import domready from 'domready';
import forEach from 'lodash';
import { gridGutter } from '../core/Settings';
import { bLazyInstance } from '../components/Figures';
import { stopAllPlayers } from '../modules/Players';
import { mobileDetect } from '../core/Mobile';


class Sliders {

    constructor() {
        this.init();
    }

    init() {
        domready( ()=> {
            this.defaultSliders();
        });
    }

    defaultSliders() {
        let sliders = document.querySelectorAll('[data-slider-default]');
        let slidersNavs = document.querySelectorAll('[data-slider-nav]');
        let sliderControls = [];

        _.forEach(sliders, (e) => {

            // get options for data attributes
            let totalSlides       = e.querySelectorAll('.swiper-slide').length;
            let spaceBetween      = 0;
            let slidesPerView     = e.getAttribute('data-slides-per-view') ? e.getAttribute('data-slides-per-view') : 'auto';
            let initialSlide      = e.getAttribute('data-slides-initial-position') ? e.getAttribute('data-slides-initial-position') : 0;
            let loop              = e.getAttribute('data-slides-loop') ? true : false;
            let effect            = e.getAttribute('data-slides-effect') ? e.getAttribute('data-slides-effect') : 'slide';
            let gridGutterBefore  = e.getAttribute('data-slides-gutter-before') ? e.getAttribute('data-slides-gutter-before') : gridGutter/2;
            let gridGutterAfter   = e.getAttribute('data-slides-gutter-after') ? e.getAttribute('data-slides-gutter-after') : 50;
            let sliderControl     = e.getAttribute('data-slider-control') ? e.getAttribute('data-slider-control') : null;
            let arrowContainer    = e.getAttribute('data-slider-arrows-container') ? document.querySelector(e.getAttribute('data-slider-arrows-container')) : null;
            let clickToSlide      = e.getAttribute('data-click-to-slide') ? e.getAttribute('data-click-to-slide') : false;

            // default arrow buttons
            let prevButton = e.querySelectorAll('.slider--arrow--prev');
            let nextButton = e.querySelectorAll('.slider--arrow--next');

            // get arrow from container
            if (arrowContainer && $(arrowContainer).is(":visible")) {
                prevButton = arrowContainer.querySelectorAll('.slider--arrow--prev');
                nextButton = arrowContainer.querySelectorAll('.slider--arrow--next');
            };

            // calculate space between slides
            if (effect == 'slide') {
                spaceBetween = mobileDetect.mobile() ? gridGutter/4 : gridGutter;
            } else if (effect == 'fade') {
                spaceBetween = 0;
            }

            // init slider
            let swiper = new Swiper(e, {
                effect: effect,
                fade: {
                    crossFade: true
                },
                loop: loop,
                initialSlide: initialSlide,
                prevButton: prevButton,
                nextButton: nextButton,
                pagination: e.querySelectorAll('.slider--pagination'),
                paginationClickable: true,
                paginationType: 'bullets',
                paginationBulletRender: function (swiper, index, className) {
                  // never returns more bullets than number of slides
                  return (index < totalSlides) ? '<span class="' + className + '"></span>' : '';
                },
                spaceBetween: spaceBetween,
                slidesOffsetBefore: gridGutterBefore,
                slidesOffsetAfter: gridGutterAfter,
                slideToClickedSlide: clickToSlide,
                slidesPerView: slidesPerView,
                breakpoints: {
                  // small tablets and mobile, hardcode to 1.5
                  768 : {
                    slidesPerView: e.getAttribute('data-slides-per-view-mobile') ? e.getAttribute('data-slides-per-view-mobile') : 1.5,
                    spaceBetweenSlides: gridGutter/2
                  }
                },
                onInit: (swiper) => {
                  $(e).addClass('ready');
                  bLazyInstance.revalidate();
                }
            });

            // revalidation image lazy loading on each slider transition start
            swiper.on('onSliderMove', (swiper) => {
              bLazyInstance.revalidate();
            });

            swiper.on('onTransitionEnd', (swiper) => {
              bLazyInstance.revalidate();
              stopAllPlayers(swiper.container);
            });

            // add to controls array
            if (sliderControl) sliderControls.push([swiper, sliderControl]);

            // remove data-slider attribute
            e.removeAttribute('data-slider-default');

        });

        // link each slider with controls
        _.forEach(sliderControls, (e) => {
            e[0].params.control = document.querySelector(e[1]).swiper;
        });

        // sliders nav
        _.forEach(slidersNavs, (e)=> {
            // get swiper
            let swiper = document.querySelector(e.getAttribute('data-slider-nav-control')).swiper;

            // go to slide on click
            $(e).find('a').on('click', (event) => {
                event.preventDefault();
                let $e = $(event.currentTarget)
                if (swiper) {
                    swiper.slideTo( $e.parent().index() );
                    // set selected
                    $e.parent().addClass('selected').siblings().removeClass('selected');
                }
            });
        });

        // set first sliderNav selected
        $(slidersNavs).find('.swiper-slide:first').addClass('selected');
    }

}


export default Sliders;
