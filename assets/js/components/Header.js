import scrollDir from "bloody-scroll-direction";

const siteHeader = "header#site-header";
const defaultminTopScroll = 100;
const headerClassName = 'filled';

class Header {

    constructor() {
        this.init();
    }

    init() {
        this.onScroll();
    }

    onScroll() {
        let scrollDirection = scrollDir.create();

        window.addEventListener('scroll', (event) => {
            if ( $(window).scrollTop() > defaultminTopScroll ) {
                $(siteHeader).addClass('fade-up');
            } else {
                $(siteHeader).removeClass('fade-up filled');
            }
        });

        // show or hide menu based on scroll direction
        scrollDirection.on("change", (callback) => {

            // scrolling down, hide menu
            if (callback.direction > 0) {
                $(siteHeader).addClass('fade-up').removeClass('filled');
            }

            // scrolling up, show menu and filled
            if (callback.direction < 0) {
                $(siteHeader).removeClass('fade-up').addClass('filled');
            }

        });
    }

    toggleHeaderStyle(scrollDirectionCallback) {
        if ( $(window).scrollTop() >= defaultminTopScroll ) {
            $(siteHeader).addClass( headerClassName );
        } else {
            $(siteHeader).removeClass( headerClassName );
        }
    }

}

/*
* Set header bar as transparent in some sections
*/
export function setHeaderTransparent() {
    $('body').addClass('header-is-transparent');
    $(siteHeader).addClass('transparent');
}

export function setHeaderFilled() {
    $(siteHeader).addClass('filled');
}

/*
* Fade in-out header, used in Player.js components
*/
export function fadeInHeader() {
    $(siteHeader).removeClass('faded-out');
}
export function fadeOutHeader() {
    $(siteHeader).addClass('faded-out');
}



export default Header;
