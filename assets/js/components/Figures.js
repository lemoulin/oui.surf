import domready from 'domready';
import Blazy from 'blazy/blazy.js';
import inlineSVG from '!!inline-svg/dist/inlineSVG.min';
import { WOW } from '!!wowjs/dist/wow.min';
import '!!vide/dist/jquery.vide';
import stripExtension from 'strip-extension';
import { mobileDetect } from '../core/Mobile';

export const bLazyInstance = new Blazy({
    offset: $(window).height(),
    loadInvisible: true,
    breakpoints: [
        {
	        width: 992,
	        src: 'data-src-md'
        },
        {
	        width: 768,
	        src: 'data-src-xs'
        }
    ],
    success: function(e) {
        $(e).addClass('loaded');
        $(e).removeClass('b-lazy');
        setTimeout(function () {
            $(e).find('img.preload-pixel').remove();
        }, 500);
    }
});

export const wowInit = function() {
    let wow = new WOW({
        boxClass:     'wow',
        animateClass: 'animated',
        offset:       -100  //added -100 to enable effect on iOS
    });
    wow.init();
};

class Figures {

    constructor() {
        this.init();
    }

    init() {
        domready( () => {
            this.svg();
            this.lazyload();
            this.reveals();
            this.videoBackgrounds();
        });
    }

    lazyload() {
        bLazyInstance.revalidate();
    }

    reveals() {
        wowInit();
    }

    videoBackgrounds() {
        parseVideoBackgrounds();
    }

    svg() {
        let svgs = inlineSVG.init({
            svgSelector: 'img.inline-svg, .svg', // the class attached to all images that should be inlined
            initClass: 'js-inlinesvg'
        }, function(e, elements) {
            // console.log( $(e) );
            $('.inlined-svg').addClass('loaded');
        });
    }

}

export function parseVideoBackgrounds() {
    // console.log("video background");
    // let elements = document.querySelectorAll('[data-video-bg]');

    $("[data-video-bg]").not('[data-initialized]').each( (index, e) => {
        let filename              = $(e).data('video-bg');
        let stripedFilename       = stripExtension(filename);
        let isHeader              = $(e).is('[class |= post-header]') || $(e).is('.serie--header') || $(e).is('.is-header');
        let $video, $src, poster  = null;

        $(e).vide(stripedFilename, {
            className : "video-bg"
        });

        // add event when video can play, which will fade in
        let videoDOM = $(e).data('vide').getVideoObject();
        $(videoDOM).attr('playsinline', '1');
        $(videoDOM).on('canplay', () => {
            $(videoDOM).addClass('canplay');
        });

        // create a vide object if not on mobile
        // if ( !mobileDetect.mobile() ) {

        //     $(e).vide(stripedFilename, {
        //         className : "video-bg"
        //     });

        //     // add event when video can play, which will fade in
        //     let videoDOM = $(e).data('vide').getVideoObject();
        //     $(videoDOM).on('canplay', () => {
        //         $(videoDOM).addClass('canplay');
        //     });

        // // on mobile, create a regular <video> element
        // } else if( mobileDetect.mobile() ) {
        //     poster  = $(e).data('video-poster');
        //     $video  = $("<video/>").attr('width', '100%').attr('height', '100%').attr('controls', 'true').attr('poster', poster).attr('playsinline', 'true').attr('muted', 'true').attr('autoplay', 'true').attr('loop', 'true');
        //     $src    = $("<source/>").attr('src', filename).attr('type', 'video/mp4').appendTo( $video );
        //     $video.appendTo( $(e) );
        // }

        // set as initialized
        $(e).attr('data-initialized', true);

    });
}

export default Figures;
