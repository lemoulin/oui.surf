import domready from 'domready';
import '!!script!Tabslet/jquery.tabslet.min';
import '!!script!sticky-kit/jquery.sticky-kit.min';
import { mobileDetect } from '../core/Mobile';
import { setHeaderFilled } from '../components/Header';
import { bLazyInstance } from '../components/Figures';

class Nav {

    constructor() {
        this.init();
    }

    init() {
        domready( ()=> {
            // for all
            this.href();
            this.externals();
            this.activateMenu();
            this.tabs();
            this.slideToggle();
            this.scrollTo();
            this.search();

            // for non mobile
            if( !mobileDetect.phone() ) this.desktopSubNav();
            if( !mobileDetect.phone() ) this.sticky();

            // for mobile
            if( mobileDetect.phone() ) this.burger();
            if( mobileDetect.mobile() ) this.mobileSubNav();
        });
    }

    // follow link for element with data-href attribute
    href() {
        $("[data-href]").on('click', function(event) {
            event.preventDefault();
            /* Act on the event */
            let href = $(this).data('href');
            window.location = href;
        });
    }

    // external links force open in new tab/window
    externals() {
        $('a').each(function() {
           var a = new RegExp('/' + window.location.host + '/');
           if(!a.test(this.href)) {
               $(this).click(function(event) {
                   event.preventDefault();
                   event.stopPropagation();
                   window.open(this.href, '_blank');
               });
           }
        });
    }

    activateMenu() {
        let path = document.location.href;
        if (path !== window.HOME_URL) $("#main-nav").find('a[href*="'+path+'"]').addClass('active');
    }

    burger() {
        $('#burger-trigger').on('click', (event) => {
            event.preventDefault();
            // let $e = $(event.currentTarget);
            $(event.currentTarget).toggleClass('open');
            $("body").toggleClass('no-scroll--y');
            this.toggleMobileNav();
        });
    }

    search() {
        $(".nav-item--search-btn, .search-bar--close").on('click', (event) => {
            event.preventDefault();
            /* Act on the event */
            this.toggleSearchBar();
        });
    }

    toggleSearchBar() {
        let $e = $("#search-bar");
        let isOpen = $e.hasClass('open') ? true : false;
        if (!isOpen) {
            $e.addClass('open');
            setTimeout(function () {
                $e.find('input[type="search"]').focus();
                bLazyInstance.revalidate();
            }, 500);
        } else {
            $e.addClass('close');
            setTimeout(function () {
                $e.removeClass('close open');
            }, 500);
        }
    }

    toggleMobileNav() {
        $("#main-nav").toggleClass('slide-in');
        setHeaderFilled();
    }

    desktopSubNav() {
        let subNavTimer = null;

        $("#main-nav").find('.menu-item').on('mouseenter', (event) => {
            let $this = $(event.currentTarget);
            $("#main-nav").find(".sub-menu--open").not($this).removeClass('sub-menu--open');
            $this.addClass('sub-menu--open');
            clearTimeout(subNavTimer);
        });

        $("#main-nav").find('.menu-item').on('mouseleave', (event) => {
            let $this = $(event.currentTarget);
            subNavTimer = setTimeout(() => {
                // $this.addClass('sub-menu--open');
                $this.siblings().removeClass('sub-menu--open');
                $this.removeClass('sub-menu--open');
            }, 250);
        });

        $("#main-nav").on('mouseleave', function(event) {
            $(".sub-menu--open").removeClass('sub-menu--open');
        });

    }

    mobileSubNav() {
        $("#main-nav").find('[data-slide-toggle-sub-nav]').on('click', (event) => {
            event.preventDefault();

            let $this = $(event.currentTarget);

            // set as open
            $this.toggleClass('open');
            $this.parent().toggleClass('open');

            // find next submenu and slide toggle element
            $this.parent().find('.sub-menu').toggleClass('open');
        });
    }

    tabs() {
        $('.tabs').each(function() {
            let $container = $(this).parent().parent();
            $(this).tabslet({
                container: $container,
                animation: true
            });

            // set container visible
            $container.addClass('ready');
        })
    }

    slideToggle() {
        $("[data-slide-toggle]").on('click', function(event) {
            event.preventDefault();
            /* Act on the event */
            let id      = $(this).attr('href');
            let $target = $( id );

            // open pannel
            $target.slideToggle(500);

            // toggle class open on all link with same #ID
            $("a[href='" + id + "']").toggleClass('open');
        });
    }

    scrollTo() {
        $('[data-scroll-to]').on('click', function(event) {
            event.preventDefault();
            let top = $( $(this).attr('href') ).offset().top;
            $("html,body").animate({ scrollTop: top }, 1000);
        });
    }

    sticky() {
        $("[data-sticky]").stick_in_parent({
            offset_top: 80
        });
    }


}


export default Nav;
