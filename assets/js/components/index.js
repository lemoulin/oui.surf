// local
import Header from './Header';
import Nav from './Nav';
import Figures from './Figures';
import Sliders from './Sliders';

const components = {
  'Header': new Header(),
  'Nav': new Nav(),
  'Figures': new Figures(),
  'Sliders': new Sliders()
};

export default components;
