import Vimeo from '!!vimeo-player-js/dist/player.min';
import YouTubePlayer from 'youtube-player';
import domready from 'domready';
import url from 'url';
import { fadeOutHeader, fadeInHeader } from "../components/Header";
import { mobileDetect } from '../core/Mobile';

// variables
const playerTrigger = "[data-video-player-trigger]";

class Players {

    constructor() {
        this.init();
    }

    init() {
        domready( ()=> {
            this.bindEvents();
            this.autoPausePlayers();
        });
    }

    bindEvents() {

        $(playerTrigger).each( (i, e) => {
            // console.log(i, e);
            let elementID         = e.getAttribute('href');
            let container         = $(elementID).get(0);
            let videoID           = $(container).data('video-id');
            let iframe            = $(elementID).find('iframe');
            let $closeBtn         = $(container).find('.btn-close-video');
            let service           = $(iframe).data('service') ? $(iframe).data('service') : 'vimeo';
            let hideNavigation    = $(e).data('video-player-hide-navigation');            

            // create video iframe objects
            let vimeoPlayerObject, youtubePlayerObject = null;

            // create vimeo API object
            if (service == 'vimeo') {
                // create vimeo player instance
                vimeoPlayerObject = new Vimeo(iframe.get(0));

                // hide when ended
                vimeoPlayerObject.on('ended', ()=> {
                    $(container).removeClass('visible');
                    $closeBtn.removeClass('visible');
                    if ( hideNavigation ) fadeInHeader();
                });

                vimeoPlayerObject.enableTextTrack('en').then(function(track) {
                    // track.language = the iso code for the language
                    // track.kind = 'captions' or 'subtitles'
                    // track.label = the human-readable label
                }).catch(function(error) {
                    switch (error.name) {
                        case 'InvalidTrackLanguageError':
                            // no track was available with the specified language
                            break;

                        case 'InvalidTrackError':
                            // no track was available with the specified language and kind
                            break;

                        default:
                            // some other error occurred
                            break;
                    }
                });

            };

            // create Youtube object
            if (service == 'youtube') {

              let player;

              // create div container for youtube new player
              let $div = $("<div/>").appendTo( $(container).find('.content') ).get(0);

              // remove all iframe
              $(container).find('iframe').remove();

              youtubePlayerObject = YouTubePlayer($div, {
                  playerVars: {
                      'autoplay': 0,
                      'controls': 1,
                      'color': 'white',
                      'hl': 'fr-ca',
                      'modestbranding': 1,
                      'showinfo': 0,
                      'rel': 0
                  }
              });
              youtubePlayerObject.loadVideoById(videoID);
              youtubePlayerObject.stopVideo();

              youtubePlayerObject.on('stateChange', (event) => {
                if (event.data == 0) {
                  $(container).removeClass('visible');
                  $closeBtn.removeClass('visible');
                  if ( hideNavigation ) fadeInHeader();
                }
              });

            }

            // play button set iframe visible and start player
            $(e).on('click', (event) => {
                event.preventDefault();

                // set iframe visible and button
                $(container).addClass('visible');
                $closeBtn.addClass('visible');

                // start vimeo player
                if (service == 'vimeo') vimeoPlayerObject.play();
                if (service == 'youtube') youtubePlayerObject.playVideo();

                // fade out site header
                if (hideNavigation) fadeOutHeader();

            });

            // register close button
            if ( $closeBtn.get(0) ) {

                $closeBtn.on('click', (event) => {
                    event.preventDefault();

                    $(event.currentTarget).removeClass('visible');

                    if ( hideNavigation ) fadeInHeader();

                    if (service == 'vimeo') {
                        vimeoPlayerObject.pause();
                    };

                    if (service == 'youtube') {
                        youtubePlayerObject.pauseVideo();
                    };

                    // hide container
                    $(container).removeClass('visible');

                });

            }

        });

        // check for autoplay
        let parseURL = url.parse(window.location.href, true);
        let autoplay = parseURL.query.autoplay ? true : false;

        // auto is set in url, start video
        autoplay && !mobileDetect.mobile() ? $(playerTrigger).eq(0).trigger('click') : null;

    }

    getEmbededServiceType(iframe) {
        let src = iframe.getAttribute('src');

        // check if Vimeo
        if ( src.search("vimeo") ) {
            return 'vimeo'
        }

        if ( src.search("youtube") ) {
            return 'youtube'
        }
    }

    // check if browser is in fullscreen mode
    isFullScreen() {
        let elements = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;
        return elements != null ? true : false;
    }

    // pause all videos
    autoPausePlayers() {
        $('iframe[data-service="vimeo"]').each( (index, e) => {
            let vimeoPlayerObject = new Vimeo(e);
            if ( vimeoPlayerObject ) vimeoPlayerObject.setAutopause(true);
        });
    }

}

export function stopAllPlayers(container = "body") {
    $(container).find('iframe[data-service="vimeo"]').each( (index, e) => {
        let vimeoPlayerObject = new Vimeo(e);
        if ( vimeoPlayerObject ) vimeoPlayerObject.pause();
    });
}

export default Players;
