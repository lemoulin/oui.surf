import JSONP from 'browser-jsonp';
import domready from 'domready';
import _ from "lodash";
const shareCountSelector  = 'data-shares-count';
const facebookGraphURL    = "https://graph.facebook.com/v2.7/";
const facebookAppToken    = "290125164442123|upsPDOtFVZFFYnPm5KbNPx6ARE8";

class SharesCount {

	constructor() {
		this.init();
	}

	init() {
		domready(()=> {
			this.counts();
		});
	}

	counts() {
		// get elements
		let elements = document.querySelectorAll(`[${shareCountSelector}]`);		

		// loop each
		_.each(elements, (e) => {
			let postURL = this.getURL(e);
			// let postURL = 'https://oui.surf/';

			// make ajax-p call
			let call = JSONP({
				url : this.getAPI_URL(postURL),
				data : { url: postURL },
				success: (data) => {
					// console.log(data);
					// let total = data.share.comment_count + data.share.share_count;
					if (data.og_object && data.og_object.engagement) {
						this.setValues(e, data.og_object.engagement.count);
						this.setCounted(e);
					}
				}
			});
		});
	}

	getAPI_URL(sourceURL) {
		return `${facebookGraphURL}?access_token=${facebookAppToken}&fields=og_object{engagement}&id=${sourceURL}`;
	}

	getURL(e) {
		let url = e.getAttribute(shareCountSelector);
		return url.length > 0 ? url : window.location.href
	}

	setValues(e, total) {
		e.textContent = total;
	}

	setCounted(e) {
		e.removeAttribute(shareCountSelector);
	}

}

export default SharesCount;
