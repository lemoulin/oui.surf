import _ from "lodash";

class SnapToSide {

	constructor() {
    this.init();
  }

	init() {
		let elements = document.querySelectorAll('[data-snap-to]');

		$(window).on('resize', () => {
			_.each(elements, (e) => {
				let $e = $(e);
				this.snap($e);
			});
		});

		// force resize on load
		$(window).trigger('resize');
	}

	snap($e) {

		let $parent  = $("body"),
		    maxWidth = $(window).width(),
				w        = $e.innerWidth(),
			  ww       = $(window).width(),
				side     = $e.data('snap-to') == null ? 'both' : $e.data('snap-to'),
				offset   = $e.offset(),
				margins  = 0,
				left     = 0,
				right    = 0;

		// // set parent if defined
		// if( $e.data('snap-to') ) {
		// 	$parent = $( $e.data('snap-to') );
		// }

		console.log(offset, ww, w);

		// save original offset
		// if ( !$e.data('offset') ) $e.data('offset', offset)

		// if (ww < maxWidth) {

			// get width from parent container
			// width = parseInt( $e.parent().outerWidth() );
			// console.log(offset);

			// append additional padding of margin from parent element
			// margins = parseInt( $e.parent().css('marginLeft') ) + parseInt( $e.parent().css('paddingLeft') );

			// // calculate left position
			left = (offset.left) * -1;
			right = (offset.left + w) - ww;
			// console.log(`right: ${right}`, `ww: ${ww}`, `w: ${w}`, (ww - (offset.left + w) ) * -1);

			// set width
			$e.css('position', 'relative');

			// set margin based on setting
			switch (side) {
				case 'both':
					$e.css('margin-left', left);
					$e.css('margin-right', right);
					break;
				case 'left':
					$e.css('margin-left', left);
					break;
				case 'right':
					$e.css('margin-right', right);
					break;
				default:

			}

		// } else {
		// 	// reset to initial values
		// 	// $e
		// 	// 	.css('width', 'auto' )
		// 	// 	.css('margin-left', 'inherit')
		// 	// 	.css('margin-right', 'inherit');
		// }

	}


}


export default SnapToSide;
