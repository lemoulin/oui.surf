/*
* Register an on scroll event with a callback behind an interval
*/

class FastOnScroll {

    constructor(interval = 3000, scrolledCallback = null, eventScope = 'scroll.fastScroll') {
        this.interval         = interval;
        this.scrolled         = false;
        this.scrolledCallback = scrolledCallback;
        this.eventScope       = eventScope;

        // init
        this.register();
        this.check();
    }

    register() {
        $(window).on(this.eventScope, (event) => {
            this.scrolled = true;
        });
    }

    check() {
        setInterval(() => {
            if( this.scrolled ) {
                this.scrolled = false;
                if( this.scrolledCallback ) this.scrolledCallback();
            }
        }, this.interval);
    }

}

export default FastOnScroll;
