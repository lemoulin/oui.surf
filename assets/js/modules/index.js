// lib
import domready from 'domready';

// local
import Players from './Players';
import Sharer from './Sharer';
import SharesCount from './SharesCount';

const modules = {
  'Players': new Players(),
  'Sharer': new Sharer(),
  'SharesCount': new SharesCount()
}

export default modules;
