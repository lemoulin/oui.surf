// import 'babel-polyfill';

// get package version
const npm_package = process.env;
const version     = npm_package.npm_package_version;
// console.log(npm_package);

// import utils
import env from 'get-env';
import notifier from 'node-notifier';
import gulp from 'gulp';
import gulpif from 'gulp-if';
import critical from 'critical';
import header from 'gulp-header';
import copy from 'gulp-copy';
import hash from 'gulp-hash';
import cssnano from 'gulp-cssnano';
import sassGlob from 'gulp-sass-glob';
import cssimport from 'gulp-cssimport';
import del from 'del';
import plumber from 'gulp-plumber';
import browserify from 'browserify';
import babelify from 'babelify';
import sass from 'gulp-sass';
import imagemin from 'gulp-imagemin';
import source from 'vinyl-source-stream';
import concat from 'gulp-concat';
import consolidate from 'gulp-consolidate';
import minifyCSS from 'gulp-minify-css';
import rename from 'gulp-rename';
import uglify from 'gulp-uglify';
import gutil from 'gulp-util';
import path from 'path';
import livereload from 'gulp-livereload';

// webpack related
import named from 'vinyl-named';
import webpack from 'webpack';
import webpackStream from 'webpack-stream';

/* Global variables */
const project_name = 'oui.surf';
const root_dir = __dirname;
const assets_dir = path.join(root_dir, 'assets');
const PROD_ENV = env() === 'dev' ? false : true;

/* DIRS */
const build_dir    = PROD_ENV ? path.join('dist', version) : path.join('dist_dev', version);
const bower_dir    = path.join(root_dir, 'bower_components');
const css_dir      = path.join(assets_dir, 'css');
const sass_dir     = path.join(assets_dir, 'sass');
const js_dir       = path.join(assets_dir, 'js');
const img_dir      = path.join(assets_dir, 'img');
const svg_dir      = path.join(assets_dir, 'svg');
const font_dir     = path.join(assets_dir, 'fonts');
const iconfont_dir = path.join(assets_dir, 'iconfont');
const main_css     = path.join(css_dir, 'main.css');
const main_sass    = path.join(sass_dir, 'main.scss');
const editor_sass  = path.join(sass_dir, 'editor-style.scss');
const admin_sass  = path.join(sass_dir, 'admin.scss');
const theme_dir    = '/wp-content/themes/oui.surf/';

/*
* Templates for gulp-header
*/

var banner = {
    theme :
    `/**
    * Theme Name: ${npm_package.npm_package_name}
    * Theme URI: ${npm_package.npm_package_homepage}
    * GitHub Theme URI: __
    * Description: ${npm_package.npm_package_description}
    * Version: ${npm_package.npm_package_version}
    * Author: ${npm_package.npm_package_author}
    * Author URI: ${npm_package.npm_package_homepage}
    * Text Domain: ouisurf
    * Domain Path: /languages
    * License: Commercial
    * Open Source Credits: None
    */`
};

/*
* cssnano supported
*/

var supported = [
    'last 2 versions',
    'safari >= 8',
    'ie >= 10',
    'ff >= 20',
    'ios 6',
    'android 4'
];


/*
* Webpack config
*/

// extractCSS config
// let extractCSS = new ExtractTextPlugin('css/[name].css', { allChunks: true });

// webpack config
var webpackConfig = {
    output: {
        filename: 'js/[name].js',
    },
    resolve: {
        modulesDirectories: ['node_modules', bower_dir],
        extensions: ['', '.webpack.js', '.web.js', '.js', '.jsx', '.json', 'scss'],
    },
    module: {
        loaders: [
            { test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel-loader' },
            // { test: /\.scss$/i, loader: extractCSS.extract(['css','sass'], { publicPath: '../'}) },
            { test: /\.json$/, loader: 'json-loader' },
            { test: /\.txt$/, loader: 'raw-loader' },
            { test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?(\?[0-9a-zA-Z]*)?$/, loader: 'url-loader?limit=10000' },
            { test: /\.(eot|ttf|wav|mp3|otf)(\?v=[0-9]\.[0-9]\.[0-9])?(\?[0-9a-zA-Z]*)?$/, loader: 'file-loader' },
        ],
    },
    externals: {
        // require("jquery") is external and available
        //  on the global var jQuery
        'jquery': 'jQuery'
    },
    plugins: [
        ...(PROD_ENV ? [
            new webpack.optimize.UglifyJsPlugin({
                compress: { warnings: false }
            })
        ] : []),
    ],
};


gulp.task('sass', function () {
    return gulp.src( [main_sass, editor_sass, admin_sass] )
    .pipe(sassGlob())
    .pipe(sass({
        includePaths: [bower_dir]
    }))
    .pipe(
        // compress CSS and auto prefix vendors only in PROD mode
        gulpif(
            PROD_ENV,
            cssnano({
              autoprefixer: {browsers: supported, add: true}
            })
        )
    )
    .on('error', function(e) {
        notifier.notify('Gulp-Sass compilation error. See console.');
        gutil.log(e);
        this.emit('end');
    })
    .pipe(gulp.dest(path.join(build_dir, 'css')))
    .pipe(livereload())
});


/*
* Scripts bundle
*/

gulp.task('scripts', function () {
    return gulp.src([
        js_dir + '/App.js'
    ])
    .pipe(named())
    .pipe(
        webpackStream(webpackConfig)
        .on('error', function(e) {
            notifier.notify('Gulp-Sass compilation error. See console.');
            gutil.log(e);
            this.emit('end');
        })
    )
    .pipe(livereload())
    .pipe(gulp.dest(build_dir));
});


/*
* Image compression
*/

gulp.task('medias:images', function() {
    return gulp.src(
        [
            path.join(img_dir, '/**/*'),
            path.join(svg_dir, '/**/*')
        ],
        { "base" : assets_dir }
    )
    .pipe(
        // compress images only in PROD mode
        gulpif(PROD_ENV, imagemin({ verbose: true }))
    )
    .on('error', function(e) {
        notifier.notify('Gulp JS compilation error. See console.');
        gutil.log(e);
        this.emit('end');
    })
    .pipe(gulp.dest( path.join(build_dir) ));
});

/*
* Various medias copy to distribution folder
*/


gulp.task('medias:clean', function() {
    return del(
        [
            path.join(build_dir, 'img/**/*'),
            path.join(build_dir, 'svg/**/*'),
            path.join(build_dir, 'fonts/**/*')
        ]
    );
});


gulp.task('medias:copy', function() {
    return gulp.src(
        [
            path.join(font_dir, '/**/*'),
        ],
        { "base" : assets_dir }
    )
    .on('error', function(e) {
        notifier.notify('Gulp JS compilation error. See console.');
        gutil.log(e);
        this.emit('end');
    })
    .pipe(gulp.dest( path.join(build_dir) ));
});


/*
* Create theme style.css
*/
gulp.task('theme', function () {
    return gulp.src( path.join(assets_dir, 'style.css') )
    .pipe(header(banner.theme))
    .pipe(gulp.dest(root_dir))
});

gulp.task('critical', function () {
    critical.generate({
        // inline: true,
        base: 'critical',
        src: 'ouisurf-source.html',
        dest: 'style-critical.css',
        width: 1300,
        height: 900
    });
});


/*
* Global tasks
* ~~~~~~~~~~~~
*/

gulp.task('build', [
    'theme',
    'medias:copy',
    'medias:images',
    'sass',
    'scripts'
]);


// watch command, run build before
gulp.task('default', ['build'], function() {
    livereload.listen({ reloadPage: true });

    // watch any less file /css directory, ** is for recursive mode
    gulp.watch(sass_dir + '/**/*.scss', ['medias:images', 'medias:copy', 'sass', 'theme']);

    // watch any less file /js directory, ** is for recursive mode
    gulp.watch(js_dir + '/**/*.js', ['scripts']);

});
