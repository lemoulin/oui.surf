<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
* Theme setup
*/
function setup() {
    $theme = wp_get_theme();
    $version = $theme->get("Version");

    // Make theme available for translation
    // Community translations can be found at https://github.com/roots/sage-translations
    load_theme_textdomain('ouisurf', get_template_directory() . '/languages');

    // Enable plugins to manage the document title
    // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
    add_theme_support('title-tag');

    // Enable post thumbnails
    // http://codex.wordpress.org/Post_Thumbnails
    // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
    // http://codex.wordpress.org/Function_Reference/add_image_size
    add_theme_support('post-thumbnails');

    // Enable post formats
    // http://codex.wordpress.org/Post_Formats
    add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

    // Enable HTML5 markup support
    // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    // Custom images sizes
    add_image_size( 'video', 704, 480, true );
    add_image_size( 'pixel', 100 );
    add_image_size( 'largest', 1800 );
    add_image_size( 'largest-2x', 2400 );

    // jepg quality
    add_filter( 'jpeg_quality', create_function( '', 'return 89;' ) );

    // Use main stylesheet for visual editor
    // To add custom styles edit /assets/styles/layouts/_tinymce.scss
    add_editor_style(Assets\asset_path( $version . '/css/editor-style.css' ));

    // add ACF Option page
    if( function_exists('acf_add_options_page') ) {
        acf_add_options_page(array(
            'page_title' 	=> 'Options du th�me OuiSurf',
            'menu_title'	=> 'Options Oui.Surf',
            'menu_slug' 	=> 'theme-general-settings'
	    ));
    }

}

add_action('after_setup_theme', __NAMESPACE__ . '\\setup');


/**
* Register Nav and Sidebars
*/
function nav_widgets_init() {

    // Register wp_nav_menu() menus
    // http://codex.wordpress.org/Function_Reference/register_nav_menus
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'ouisurf'),
        'footer_nav' => __('Footer Navigation', 'ouisurf'),
        'language_nav' => __('Language Navigation', 'ouisurf'),
        'social_nav' => __('Social links Navigation', 'ouisurf'),
        'about_nav' => __('About page Navigation', 'ouisurf')
    ]);

    register_sidebar([
        'name'          => __('Primary', 'ouisurf'),
        'id'            => 'sidebar-primary',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ]);

    register_sidebar([
        'name'          => __('Homepage sidebar', 'ouisurf'),
        'id'            => 'sidebar-homepage',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ]);

    // register_sidebar([
    //     'name'          => __('Footer Nav', 'ouisurf'),
    //     'id'            => 'sidebar-footer-nav',
    //     'before_widget' => '<section class="widget %1$s %2$s">',
    //     'after_widget'  => '</section>',
    //     'before_title'  => '<h3>',
    //     'after_title'   => '</h3>'
    // ]);

    register_sidebar([
        'name'          => __('Footer Newsletter', 'sage'),
        'id'            => 'sidebar-footer-newsletter',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ]);

    // register_sidebar([
    //     'name'          => __('Footer Social links', 'sage'),
    //     'id'            => 'sidebar-footer-nav-social',
    //     'before_widget' => '<section class="widget %1$s %2$s">',
    //     'after_widget'  => '</section>',
    //     'before_title'  => '<h3>',
    //     'after_title'   => '</h3>'
    // ]);

    register_sidebar([
        'name'          => __('Archives ads', 'ouisurf'),
        'id'            => 'archives-ads',
        'before_widget' => '<div class="%1$s %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => ''
    ]);

    register_sidebar([
        'name'          => __('BigBox', 'ouisurf'),
        'id'            => 'bigbox',
        'before_widget' => '<div class="%1$s %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => ''
    ]);

}

add_action('widgets_init', __NAMESPACE__ . '\\nav_widgets_init');

/**
* Determine which pages should NOT display the sidebar
*/
function display_sidebar() {
    static $display;

    isset($display) || $display = !in_array(true, [
        // The sidebar will NOT be displayed if ANY of the following return true.
        // @link https://codex.wordpress.org/Conditional_Tags
        is_404(),
        is_front_page(),
        is_page_template('template-custom.php'),
    ]);

    return apply_filters('sage/display_sidebar', $display);
}

/**
* Theme assets
*/
function assets() {
    $theme = wp_get_theme();
    $version = $theme->get("Version");

    // wp_enqueue_style('sage/css_critical', Assets\asset_path("$version/css/style-critical.css"), false, null);
    wp_enqueue_style('sage/css', Assets\asset_path("$version/css/main.css"), false, null);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    // wp_enqueue_script('sage/vimeo', "https://player.vimeo.com/api/player.js", null, true);
    wp_enqueue_script('sage/youtube', "https://www.youtube.com/iframe_api", null, true);
    wp_enqueue_script('sage/jquery', Assets\asset_path("vendors/jquery/jquery.min.js"), null, true);
    wp_enqueue_script('sage/js', Assets\asset_path("$version/js/App.js"), null, true);

    // dequeue plugin css/js, code sample
    wp_deregister_script('jquery');
    wp_deregister_script('jquery-migrate');
    wp_dequeue_style('yarppWidgetCss');
}

add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);
