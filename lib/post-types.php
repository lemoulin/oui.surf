<?php

namespace Roots\Sage\PostTypes;

function post_types() {

	// Sliders
	register_post_type('sliders',
		array(
			'labels' => array(
				'name' => __( 'Sliders' ),
				'singular_name' => __( 'Slider' )
			),
			'public' => true,
			'publicly_queryable' => false,
			'has_archive' => false,
			'rewrite' => array('slug' => 'sliders'),
			'menu_position' => 5,
			'supports' => array('title', 'thumbnail', 'custom-fields')
		)
	);

	// Guides
	// register_post_type('guide',
	// 	array(
	// 		'labels' => array(
	// 			'name' => __( 'Guides (ancien)' ),
	// 			'singular_name' => __( 'Guide (ancien)' )
	// 		),
	// 		'public' => true,
	// 		'publicly_queryable' => true,
	// 		'has_archive' => true,
	// 		'rewrite' => array('slug' => 'old_guides'),
	// 		'menu_position' => 5,
	// 		'supports' => array('title', 'editor', 'author', 'thumbnail', 'custom-fields', 'post-formats')
	// 	)
	// );

	// Webtele
	// register_post_type('webtele',
	// 	array(
	// 		'labels' => array(
	// 			'name' => __( 'Vidéos (ancien)' ),
	// 			'singular_name' => __( 'Vidéo (ancien)' )
	// 		),
	// 		'public' => true,
	// 		'publicly_queryable' => true,
	// 		'has_archive' => true,
	// 		'rewrite' => array('slug' => 'webtele'),
	// 		'menu_position' => 6,
	// 		'show_ui' => true,
	// 		'supports' => array('title', 'editor', 'author', 'thumbnail', 'custom-fields', 'post-formats')
	// 	)
	// );

	// Journal
	// register_post_type('journal',
	// 	array(
	// 		'labels' => array(
	// 			'name' => __( 'Journaux (ancien)' ),
	// 			'singular_name' => __( 'Journal (ancien)' )
	// 		),
	// 		'public' => true,
	// 		'publicly_queryable' => true,
	// 		'has_archive' => true,
	// 		'rewrite' => array('slug' => 'journal'),
	// 		'menu_position' => 7,
	// 		'supports' => array('title', 'editor', 'author', 'thumbnail', 'custom-fields', 'post-formats')
	// 	)
	// );

	// Capsule
	// register_post_type('capsule',
	// 	array(
	// 		'labels' => array(
	// 			'name' => __( 'Capsules (ancien)' ),
	// 			'singular_name' => __( 'Capsule (ancien)' )
	// 		),
	// 		'public' => true,
	// 		'publicly_queryable' => true,
	// 		'has_archive' => true,
	// 		'rewrite' => array('slug' => 'capsule'),
	// 		'menu_position' => 7,
	// 		'supports' => array('title', 'editor', 'author', 'thumbnail', 'custom-fields', 'post-formats')
	// 	)
	// );

	// Important or else permalinks won't work
	flush_rewrite_rules();

}

add_action('init', __NAMESPACE__ . '\\post_types');


?>
