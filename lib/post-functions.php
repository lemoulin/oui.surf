<?php

/**
 * Set a share image in Serie taxonomies
 */

function serie_share_image() {
    if ( is_tax('series') ) {
        // get current category term ID
        $taxonomy_id = get_queried_object_id();
        $acf_query_selector = "series_$taxonomy_id";
        $header_image = get_field('main_image', $acf_query_selector);
        if ($header_image) {
            echo "<meta property='og:image' content='" . $header_image['sizes']['large'] . "' />\n";
            // echo "<meta property='og:description' content='" . wp_strip_all_tags(get_the_archive_description()) . "' />";
        }
    }
}

add_filter('wp_head', 'serie_share_image', 5 );

/**
 * Add new rewrite rule
 */
function ouisurf_old_blog_url_querystring() {
    add_rewrite_rule(
        'blogue/([^/]*)$',
        'index.php?name=$matches[1]',
        'top'
    );
    add_rewrite_rule(
        'blogue/category/([^/]*)$',
        'index.php?category_name=$matches[1]',
        'top'
    );
    add_rewrite_rule(
        'blogue/tag/([^/]*)$',
        'index.php?tag=$matches[1]',
        'top'
    );
    add_rewrite_tag('%blogue%','([^/]*)');
}
add_action('init', 'ouisurf_old_blog_url_querystring', 999 );

/**
 * Custom search form
 */
function ouisurf_search_form($form) {
    $form = '<form role="search" method="get" id="search-form" class="search-form" action="' . home_url( '/' ) . '" >
    <div><label class="screen-reader-text" for="s">' . __( 'Search for:' ) . '</label>
    <input type="search" value="' . get_search_query() . '" name="s" id="s" placeholder="' . __("Rechercher sur OuiSurf", "ouisurf") . '" />
    <input type="submit" id="search-submit" class="search-submit" value="' . __("Rechercher", "ouisurf") . '" />
    </div>
    </form>';
    return $form;
}
add_filter( 'get_search_form', 'ouisurf_search_form' );

/**
 * Custom get archive title
 */

function ouisurf_get_the_archive_title() {
    if ( is_category() ) {
        $title = sprintf( __( '%s' ), single_cat_title( '', false ) );
    } elseif ( is_tag() ) {
        $title = sprintf( __( 'Étiquette : %s' ), single_tag_title( '', false ) );
    } elseif ( is_author() ) {
        $title = sprintf( __( 'Auteur : %s' ), '<span class="vcard">' . get_the_author() . '</span>' );
    } elseif ( is_year() ) {
        $title = sprintf( __( 'Année : %s' ), get_the_date( _x( 'Y', 'yearly archives date format' ) ) );
    } elseif ( is_month() ) {
        $title = sprintf( __( 'Mois : %s' ), get_the_date( _x( 'F Y', 'monthly archives date format' ) ) );
    } elseif ( is_day() ) {
        $title = sprintf( __( 'Jour : %s' ), get_the_date( _x( 'F j, Y', 'daily archives date format' ) ) );
    } elseif ( is_tax( 'post_format' ) ) {
        if ( is_tax( 'post_format', 'post-format-aside' ) ) {
            $title = _x( 'Asides', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
            $title = _x( 'Galleries', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
            $title = _x( 'Images', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
            $title = _x( 'Videos', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
            $title = _x( 'Quotes', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
            $title = _x( 'Links', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
            $title = _x( 'Statuses', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
            $title = _x( 'Audio', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
            $title = _x( 'Chats', 'post format archive title' );
        }
    } elseif ( is_post_type_archive() ) {
        $title = sprintf( __( 'Archives: %s' ), post_type_archive_title( '', false ) );
    } elseif ( is_tax() ) {
        $tax = get_taxonomy( get_queried_object()->taxonomy );
        /* translators: 1: Current taxonomy term */
        $title = sprintf( __( '%1$s' ), single_term_title( '', false ) );
    } else {
        $title = __( 'Archives' );
    }

    /**
     * Filter the archive title.
     *
     * @since 4.1.0
     *
     * @param string $title Archive title to be displayed.
     */
    return apply_filters( 'ouisurf_get_the_archive_title', $title );
}


/**
 * Custom oembed filter, add a wrapper for responsive embeded iframes
 */

function ouisurf_oembed_filter($html, $url, $attr, $post_ID) {
    $return = '<div class="embed-responsive embed-responsive-16by9">'.$html.'</div>';
    return $return;
}

add_filter( 'embed_oembed_html', 'ouisurf_oembed_filter', 10, 4 ) ;

/**
* Filter the except length to 20 characters.
*
* @param int $length Excerpt length.
* @return int (Maybe) modified excerpt length.
*/
function ouisurf_custom_excerpt_length() {
    return 30;
}

add_filter( 'excerpt_length', 'ouisurf_custom_excerpt_length' );

/**
* Filter the excerpt "read more" string.
*
* @param string $more "Read more" excerpt string.
* @return string (Maybe) modified "read more" excerpt string.
*/
function ouisurf_excerpt_more( ) {
    return '...';
}
add_filter( 'excerpt_more', 'ouisurf_excerpt_more' );



if ( ! function_exists( 'ouisurf_post_get_section' ) ) :
    /**
    * Get post current ouisurf_section category
    * return first found, ignore others
    */
    function ouisurf_post_get_section($post, $slug = True, $before = Null, $after = Null, $check_serie = false) {
        $taxonomy = "ouisurf_section";
        $taxonomy_terms = get_terms( $taxonomy );

        // first and foremost, check if post is a in series
        if ( $check_serie && ouisurf_is_serie($post) ) {

            return "series";
            // break;

            // not a serie, find section
        } else {

            foreach ($taxonomy_terms as $term) {
                if ( has_term($term->term_id, $taxonomy, $post) ) {
                    return $slug ? "{$before}{$term->slug}{$after}" : "{$before}{$term->name}{$after}";
                    break;
                }
            }

        }

        // none found
        return null;
    }
endif;



if ( ! function_exists( 'ouisurf_is_serie' ) ) :
    /**
    * Return post-thumbnail image URL
    */
    function ouisurf_is_serie($post) {
        $taxonomy = "ouisurf_section";
        $has_serie = has_term("series", $taxonomy, $post);
        if ( $has_serie ) {

            return true;

        } else {

            return false;

        }
    }
endif;


if ( ! function_exists( 'ouisurf_post_get_post_thumbnail_url' ) ) :
    /**
    * Return post-thumbnail image URL
    */
    function ouisurf_post_get_post_thumbnail_url($postID, $size = 'large', $placeholder = True) {
        // default thumbnail
        $default_thumbnail = get_field('default_thumbnail', 'options');

        // get thumbnail image attachement ID
        $imageID = get_post_thumbnail_id( $postID );

        // find the image src
        $image = wp_get_attachment_image_src($imageID, $size, false);

        /**
        * Returns image url stored in first array position :
        * Example: Array([0] => http://oui.surf.dev/wp-content/uploads/2016/06/0Y5A9768.jpg)
        */
        if ( $image && isset($image[0]) ) {
            return $image[0];
        } else if( !$image && $placeholder ) {
            return $default_thumbnail['sizes']['large'];
        } else {
            return null;
        }
    }
endif;

/**
 * Output gallery in a custom format
*/

add_filter('post_gallery','ouisurf_custom_gallery', 10, 2);

function ouisurf_custom_gallery($output = '', $atts) {
    // get all gallery shortcode posts
    $posts = get_posts(array('include' => $atts['ids'], 'orderby' => $atts['orderby'], 'post_type' => 'attachment'));
    $total = count($posts);

    // slider wrapper begin
    $output = "<div data-slider-default data-click-to-slide='true' data-slides-gutter-before='0' data-slides-per-view-mobile='auto' class='swiper-container slider--default slider--gallery' ><div class='swiper-wrapper'>";

    // slide for each image
	foreach($posts as $key => $image){
        $position = ++$key;
    	if ($image->post_excerpt) {
    		$caption = "<figcaption class='caption'><ins>$position/$total</ins><p>" . $image->post_excerpt . "</p></figcaption>";
    	} else {
    		$caption = "<figcaption class='caption'><ins>$position/$total</ins></figcaption>";
    	}
    	// <img>
    	$img = wp_get_attachment_image_src($image->ID, 'largest');
    	$img = $img[0];
    	// create and append html
        $output .= "<div class='swiper-slide slider--image'><figure><picture><img src='".$img."'></picture>$caption</div>";
    }

    $output .= "</div><nav class='swiper-pagination slider--pagination'></nav></div>";

    return $output;
}




if ( ! function_exists( 'ouisurf_post_get_content_type' ) ) :
    /**
    * Get post current serie taxonony
    * return first found, ignore others
    */
    function ouisurf_post_get_serie($post, $slug = True) {
        $taxonomy = "series";
        $taxonomy_terms = get_terms( $taxonomy );

        foreach ($taxonomy_terms as $term) {
            if ( has_term($term->term_id, $taxonomy, $post) ) {
                return $slug ? $term->slug : $term;
                break;
            }
        }

        // none found
        return null;
    }
endif;




/**
* Increment post view
*/
function ouisurf_set_post_views($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }

    return $count;
}


/*
* Post view count AJAX
*/

function increment_post_view_javascript() {
    if ( is_singular() ) :
        ?>
        <script type="text/javascript" >
        jQuery(document).ready(function($) {
            // ajaxurl is defined in admin, not in front end
            var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
            var post_id = <?php the_ID(); ?>;

            var data = {
                'action': 'increment_post_view',
                'post_id': post_id
            };

            $.post(ajaxurl, data, function(response) {
                console.warn('Views for post ID ' + post_id + ': ' + response);
            });
        });
        </script>
        <?php
    endif;
}

add_action( 'wp_footer', 'increment_post_view_javascript' );

function increment_post_view_callback() {
    $postID = intval( $_POST['post_id'] );
    $view_count = ouisurf_set_post_views($postID);
    echo $view_count;
    wp_die(); // this is required to terminate immediately and return a proper response
}

add_action( 'wp_ajax_increment_post_view', 'increment_post_view_callback' );
add_action( 'wp_ajax_nopriv_increment_post_view', 'increment_post_view_callback' );


?>
