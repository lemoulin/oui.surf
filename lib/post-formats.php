<?php

namespace Roots\Sage\PostFormats;

/**
 * Add custom taxonomies
 *
 * Additional custom taxonomies can be defined here
 * http://codex.wordpress.org/Function_Reference/register_taxonomy
 */
// add_action( 'init', 'ouisurf_custom_post_formats', 0 );

function custom_post_formats() {

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats',
		array(
			'video',
			'gallery',
			// 'aside',
			// 'audio',
		),
		array(
			'post',
			'webtele'
		)
	);

}

add_action('init', __NAMESPACE__ . '\\custom_post_formats');

?>
