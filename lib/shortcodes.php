<?php 

function get_flights_deals() {
  $curl = curl_init( 'http://107.170.242.227/ouisurf' );
  curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
  $response = curl_exec( $curl );
  curl_close( $curl );
  if ($response) {
    return $response;
  }
}

/**
 * 
 */
function ouisurf_flights_deals($atts, $content = null)
{
    ob_start();
?>
      <section class="flights-deals">
        <p>
          <?= get_flights_deals() ?>
        </p>
      </section>
 <?php
    return ob_get_clean();
}
add_shortcode("flights_deals", "ouisurf_flights_deals");

?>