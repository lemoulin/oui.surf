<?php

namespace Roots\Sage\Taxonomies;

/**
 * Add custom taxonomies
 *
 * Additional custom taxonomies can be defined here
 * http://codex.wordpress.org/Function_Reference/register_taxonomy
 */

function custom_taxonomies() {

	// Add "Ouisuif section type" taxonomy to Posts
	register_taxonomy('ouisurf_section', array('post'), array(
		'hierarchical' => true,
		'query_var' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,

		// This array of options controls the labels displayed in the WordPress Admin UI
		'labels' => array(
			'name' => _x( 'Section', 'taxonomy general name' ),
			'singular_name' => _x( 'Section', 'taxonomy singular name' ),
			'search_items' =>  __( 'Chercher dans les sections' ),
			'all_items' => __( 'Toutes les sections' ),
			'parent_item' => __( 'Section parente' ),
			'parent_item_colon' => __( 'Type de section parente:' ),
			'edit_item' => __( 'Modifier Section' ),
			'update_item' => __( 'Modifier Section' ),
			'add_new_item' => __( 'Ajouter un Section ou une rubrique' ),
			'new_item_name' => __( 'Nouvelle Section' ),
			'menu_name' => __( 'Sections' ),
		),

		// Control the slugs used for this taxonomy
		'rewrite' => array(
			'slug' => 'section', // This controls the base slug that will display before each term
			'with_front' => false, // Don't display the category base before "/ouisurf_section_type/"
			'hierarchical' => true // This will allow URL's like "/ouisurf_section_type/cat-name/cat-slug/"
		),
	));

	// default categories for each sections

	if (! term_exists('Articles', 'ouisurf_section')) {
		wp_insert_term(
			'Articles',
			'ouisurf_section',
			array(
				'description'=> 'Billets texte',
				'slug' => 'articles',
			)
		);
	};

	if (! term_exists('Series', 'ouisurf_section')) {
		wp_insert_term(
			'Séries',
			'ouisurf_section',
			array(
				'description'=> 'Séries videos',
				'slug' => 'series',
			)
		);
	};

	if (! term_exists('Vidéos', 'ouisurf_section')) {
		wp_insert_term(
			'Vidéos',
			'ouisurf_section',
			array(
				'description'=> 'Vidéos',
				'slug' => 'videos',
			)
		);
	};

	if (! term_exists('Guides', 'ouisurf_section')) {
		wp_insert_term(
			'Guides',
			'ouisurf_section',
			array(
				'description'=> 'Guides',
				'slug' => 'guides',
			)
		);
	};

	if (! term_exists('Dossiers', 'ouisurf_section')) {
		wp_insert_term(
			'Dossiers',
			'ouisurf_section',
			array(
				'description'=> 'Dossiers, longform articles.',
				'slug' => 'dossiers',
			)
		);
	};


	// Add "Ouisuif web série" taxonomy
	register_taxonomy('series', array('post', 'webtele'), array(
		'hierarchical' => true,
		// 'query_var' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,

		// This array of options controls the labels displayed in the WordPress Admin UI
		'labels' => array(
			'name' => _x( 'Série', 'taxonomy general name' ),
			'singular_name' => _x( 'Série', 'taxonomy singular name' ),
			'search_items' =>  __( 'Chercher dans les séries' ),
			'all_items' => __( 'Toutes les séries' ),
			'parent_item' => __( 'Série parente' ),
			'parent_item_colon' => __( 'Type de section parente:' ),
			'edit_item' => __( 'Modifier Série' ),
			'update_item' => __( 'Modifier Série' ),
			'add_new_item' => __( 'Ajouter un Série ou une rubrique' ),
			'new_item_name' => __( 'Nouvelle Série' ),
			'menu_name' => __( 'Séries vidéos' ),
		),

		// Control the slugs used for this taxonomy
		'rewrite' => array(
			'slug' => 'series', // This controls the base slug that will display before each term
			'with_front' => false, // Don't display the category base before "/ouisurf_section_type/"
			'hierarchical' => true // This will allow URL's like "/ouisurf_section_type/cat-name/cat-slug/"
		),
	));

	// Add "Ouisuif web série" taxonomy
	register_taxonomy('videos', array('post', 'webtele'), array(
		'hierarchical' => true,
		'query_var' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,

		// This array of options controls the labels displayed in the WordPress Admin UI
		'labels' => array(
			'name' => _x( 'Catégorie videos', 'taxonomy general name' ),
			'singular_name' => _x( 'Catégorie vidéos', 'taxonomy singular name' ),
			'search_items' =>  __( 'Chercher dans les catégorie vidéos' ),
			'all_items' => __( 'Toutes les catégorie vidéos' ),
			'parent_item' => __( 'Catégorie vidéos parente' ),
			'parent_item_colon' => __( 'Type de catégorie vidéos parente:' ),
			'edit_item' => __( 'Modifier Catégorie vidéos' ),
			'update_item' => __( 'Modifier Catégorie vidéos' ),
			'add_new_item' => __( 'Ajouter un catégorie video ou une rubrique' ),
			'new_item_name' => __( 'Nouvelle catégorie vidéo' ),
			'menu_name' => __( 'Catégorie de vidéos' ),
		)
	));



	// Add "Ouisuif season" taxonomy
	// XXX : to remove later
	register_taxonomy('season', 'webtele', array(
		// Hierarchical taxonomy (like categories)
		'hierarchical' => true,
		'query_var' => true,

		// This array of options controls the labels displayed in the WordPress Admin UI
		'labels' => array(
			'name' => _x( 'Saison (ancien)', 'taxonomy general name' ),
			'singular_name' => _x( 'Saison', 'taxonomy singular name' ),
			'search_items' =>  __( 'Chercher dans les sections' ),
			'all_items' => __( 'Toutes les sections' ),
			'parent_item' => __( 'Saison parente' ),
			'parent_item_colon' => __( 'Type de section parente:' ),
			'edit_item' => __( 'Modifier Saison' ),
			'update_item' => __( 'Modifier Saison' ),
			'add_new_item' => __( 'Ajouter un Saison ou une rubrique' ),
			'new_item_name' => __( 'Nouvelle Saison' ),
			'menu_name' => __( 'Saisons' ),
		)
	));

	// Add "Ouisuif guide_category" taxonomy
	register_taxonomy('guide_category', array('post', 'guide'), array(
		// Hierarchical taxonomy (like categories)
		'hierarchical' => true,
		'query_var' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,

		// This array of options controls the labels displayed in the WordPress Admin UI
		'labels' => array(
			'name' => _x( 'Catégorie Guide', 'taxonomy general name' ),
			'singular_name' => _x( 'Catégories Guide', 'taxonomy singular name' ),
			'search_items' =>  __( 'Chercher dans les catégories guide' ),
			'all_items' => __( 'Toutes les catégories guide' ),
			'parent_item' => __( 'Catégorie Guide parente' ),
			'parent_item_colon' => __( 'Type de catégorie guide parente:' ),
			'edit_item' => __( 'Modifier Catégorie Guide' ),
			'update_item' => __( 'Modifier Catégorie Guide' ),
			'add_new_item' => __( 'Ajouter un Catégorie Guide ou une rubrique' ),
			'new_item_name' => __( 'Nouvelle Catégorie Guide' ),
			'menu_name' => __( 'Catégorie Guides' ),
		)

	));


	flush_rewrite_rules();
}

add_action('init', __NAMESPACE__ . '\\custom_taxonomies');

?>
