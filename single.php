<?php while (have_posts()) : the_post(); ?>

    <article <?php post_class(); ?> data-permalink="<?php the_permalink() ?>" data-controller="Post" data-action="<?php echo ouisurf_post_get_section($post, true, null, null, true) ?>" data-ouisurf-section="<?php echo ouisurf_post_get_section($post) ?>">

        <?php get_template_part('templates/content-single', ouisurf_post_get_section($post, true, null, null, true)); ?>

        <?php // related posts ?>
        <footer class="post-footer">
            <?php get_template_part('templates/partials/related-posts') ?>
        </footer>

    </article>

<?php endwhile; ?>
