<?php

use Roots\Sage\Titles;

$taxonomy_id = get_queried_object_id();
$queried_object = get_queried_object();

$posts_per_page = get_option( 'posts_per_page' );

// Features posts
$recent_posts = Array();
isset($posts[0]) ? $recent_posts[0] = $posts[0] : null;
isset($posts[1]) ? $recent_posts[1] = $posts[1] : null;

// get sticky posts for current category or 'ouisurf_section' custom taxonomy
$sticky_posts_args = array(
	'posts_per_page' => -1,
    'post_type' => 'post',
	'post__in'  => get_option( 'sticky_posts' ),
    'tax_query' => array(
		'relation' => 'OR',
		array(
			'taxonomy' => 'category',
			'field'    => 'id',
			'terms'    => array($taxonomy_id),
			'operator' => 'IN',
		),
		array(
			'taxonomy' => 'ouisurf_section',
			'field'    => 'id',
			'terms'    => array($taxonomy_id),
			'operator' => 'IN'
		)
	)
);
$sticky_posts = new WP_Query( $sticky_posts_args );

?>

<section class="archive-default">

    <header class="archive-header">
    	<div class="content">
    		<h2 class="archive-title"><?= Titles\title(); ?></h2>

            <?php if (isset($recent_posts[0])): ?>
    		<?php $post = $recent_posts[0] ?>
    		<?php get_template_part('templates/featured', ouisurf_post_get_section($post)); ?>
            <?php endif; ?>

            <?php if (isset($recent_posts[1])): ?>
    		<?php $post = $recent_posts[1] ?>
    		<?php get_template_part('templates/featured', ouisurf_post_get_section($post)); ?>
            <?php endif; ?>

    	</div>
    </header>

    <?php if ($sticky_posts->post_count >= 3): ?>
    <div class="archive--sticky-posts container-fluid container-fluid--max-width-xl">
        <div class="row">

            <header class="col-xs-12 section--sep">
                <h4 class="no-margin--top">Choix de l'équipe</h4>
				<nav id="slider-controls-sticky" class="section--sep--menu">
		            <a href="#" class="slider--arrow--prev"><i class="ion-ios-arrow-left"></i></a>
		            <a href="#" class="slider--arrow--next"><i class="ion-ios-arrow-right"></i></a>
		        </nav>
            </header>

            <div class="col-xs-12 no-padding--left">

                <div data-slider-default data-slides-per-view="5.25" data-slider-arrows-container="#slider-controls-sticky" class="swiper-container slider--default">
                    <div class="swiper-wrapper">

                    <?php while ($sticky_posts->have_posts()) : $sticky_posts->the_post(); ?>
                    <?php if ($post->ID != $recent_posts[0]->ID && $post->ID != $recent_posts[1]->ID): ?>
                        <div class="swiper-slide">
                            <?php get_template_part('templates/small'); ?>
                        </div>
                    <?php endif; ?>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata() ?>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <?php endif; ?>

    <div class="container-fluid container-fluid--max-width-xl">
        <div class="row">

            <div class="archive-posts">
                <?php get_template_part('templates/partials', 'no-results'); ?>

                <?php $i = 1; while (have_posts()) : the_post(); ?>

                    <?php if ($post->ID != $recent_posts[0]->ID && $post->ID != $recent_posts[1]->ID): ?>
                    <?php get_template_part('templates/compact', ouisurf_post_get_section($post)); ?>

                    <!-- ads -->
                    <?php if ($i == ($posts_per_page / 2)): ?>
                    <div class="archives-ads">
                        <?php dynamic_sidebar( 'archives-ads' ); ?>
                    </div>
                    <?php endif ?>

                    <?php endif; ?>

                <?php $i++; endwhile; ?>

				<?php the_posts_navigation(array("prev_text" => 'Page suivante <i class="ion-ios-arrow-right"></i>', "next_text" => '<i class="ion-ios-arrow-left"></i> Page Précédente')) ?>

            </div>

            <aside class="archive-sidebar" data-sticky>
                <?php dynamic_sidebar('sidebar-primary'); ?>
				<?php get_template_part("templates/partials/sidebar-sticky-posts") ?>
            </aside>

        </div>
    </div>
    <!-- /.container  -->

</section>
