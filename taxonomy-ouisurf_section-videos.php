<?php

use Roots\Sage\Titles;

/**
* Videos landing page
*/

$most_recent = isset( $wp_query->posts[0] ) ? $wp_query->posts[0] : null;

// get current taxonomy query objects
$taxonomy = get_queried_object();

?>

<section class="section-videos" data-controller='SectionVideos'>

    <header class="post-header--videos">

        <div class="container">

            <div class="row visible-xs">
                <header class="col-xs-12 section--sep border-bottom">
                    <h2 class="h4"><?php _e( "Vidéo plus récente", "ouisurf" ) ?></h2>
                </header>
            </div>

            <div class="row">

                <!-- video content -->
                <aside class="col-sm-8 player">
                    <?php if ($most_recent): ?>
                        <?php hm_get_template_part('templates/content-type/videos/video-player', array('post' => $most_recent)); ?>
                    <?php endif; ?>
                </aside>

                <!-- Recents and popular videos -->
                <?php wp_reset_postdata(); ?>
                <?php get_template_part('templates/content-type/videos/nav-recents-populars'); ?>

            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->

    </header>

    <!-- most recents and popular posts sliders -->
    <div class="container section-video--posts-index">

        <div class="row">
            <header class="col-xs-12 section--sep">
                <?php if ($taxonomy->taxonomy == 'ouisurf_section'): ?>
                <h4><?php _e( "Toutes les vidéos", "ouisurf" ) ?></h4>
                <?php else: ?>
                <h4><?php _e( "Vidéos", "ouisurf" ) ?> : <?= Titles\title(); ?></h4>
                <?php endif; ?>
                <!-- trigger categories menu -->
                <nav class="section--sep--menu">
                    <a href="#"><?php _e( "Catégories", "ouisurf" ) ?> <i class="ion-ios-arrow-down"></i></a>
                </nav>
                <!-- categories -->
                <ul class="section--sep--menu--items list-unstyled">
                    <?php wp_list_categories('taxonomy=videos&title_li&hide_empty=1') ?>
                </ul>
            </header>
        </div>

        <div class="row">

            <!-- recents posts -->
            <?php while (have_posts()) : the_post(); ?>
                <?php if ($post->ID != $most_recent->ID): ?>
                <div class="col-xs-12 col-sm-4">
                    <?php get_template_part('templates/compact-video'); ?>
                </div>
                <?php endif; ?>
            <?php endwhile; ?>

            <!-- /end recents posts  -->
            <?php wp_reset_postdata(); ?>


        </div>
        <!-- /end .row -->

        <?php
            the_posts_navigation(
                array(
                    "prev_text" => sprintf( "%s <i class='ion-ios-arrow-right'></i>", __("Page suivante", "ouisurf") ),
                    "next_text" => sprintf( "<i class='ion-ios-arrow-left'></i> %s", __("Page précédente", "ouisurf") )
                )
            )
        ?>

    </div>
    <!-- end .container -->


</section>
