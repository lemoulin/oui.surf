<?php

$pageID = get_field('404_page', 'options');
if ($pageID) {
    $post = get_page( $pageID );
    $content = apply_filters('the_content', $post->post_content);
}

?>

<article <?php post_class(); ?> data-controller="Post" data-action="pages" data-ouisurf-section="<?php echo $post->slug ?>">

    <?php get_template_part('templates/post-formats/post-header'); ?>

    <div id="entry-content" class="entry-content--pages">

        <!-- entry main content -->
        <div class="container">

            <div class="row">

                <aside class="col-sm-2">
                    <?php // empty ?>
                </aside>

                <div class="col-sm-8 entry-body">
                    <?php echo $content; ?>
                </div>

            </div>

        </div>
        <!-- /.container -->

    </div>

</article>
