<?php
/**
 * Series landing page
 */
?>

<section class="section-series" data-controller='SectionSeries'>

  <?php get_template_part('templates/content-type/series/featured-series-taxonomy'); ?>

  <!-- most recents and popular posts sliders -->
  <div class="container-fluid section-video--posts-sliders">

    <div class="row">
      <hgroup class="col-md-offset-1 col-md-11 col-xs-12 section--sep">
        <h4><?php _e( "Vidéos populaires", "ouisurf" ) ?></h4>
        <nav id="slider-controls-most-populars" class="section--sep--menu visible-xs">
            <a href="#" class="slider--arrow--prev"><i class="ion-ios-arrow-left"></i></a>
            <a href="#" class="slider--arrow--next"><i class="ion-ios-arrow-right"></i></a>
        </nav>
      </hgroup>
    </div>

    <div class="row">

      <?php
      /**
       * Most popular video posts
       * Get all posts less than 4 months old
       */
      $most_populars_args = array(
        'posts_per_page' => -1,
        'taxonomy' => 'ouisurf_section',
        'field' => 'slug',
        'term' => 'series',
        'meta_key' => 'post_views_count',
        'orderby' => 'meta_value_num',
        'order' => 'DESC',
        'post_status'  => 'publish',
        'date_query' => array(
          array(
            'after' => '28 months ago'
          )
        )
      );

      $popular_posts = new WP_Query($most_populars_args);

      ?>

      <div class="col-md-offset-1 col-md-11 col-xs-12 no-padding--right--min-sm">

        <!-- popular posts -->
        <div data-slider-default data-slides-gutter-before="0" data-slides-per-view="3.25" data-slides-per-view-mobile="1.11" data-slider-arrows-container="#slider-controls-most-populars" class="swiper-container slider--default serie--posts-slider">
          <a href="#" class="slider--arrow--prev in-circle hidden-xs"><i class="ion-ios-arrow-left"></i></a>
          <a href="#" class="slider--arrow--next in-circle hidden-xs"><i class="ion-ios-arrow-right"></i></a>
          <div class="swiper-wrapper">
            <?php while ($popular_posts->have_posts()) : $popular_posts->the_post(); ?>
              <div class="swiper-slide">
                <?php get_template_part('templates/compact-video'); ?>
              </div>
            <?php endwhile; ?>
          </div>
        </div>
        <!-- /end popular posts  posts  -->

        <?php wp_reset_postdata(); ?>

      </div>

    </div>
    <!-- /end .row -->

    <?php
    /**
     * All sticky series posts
     */
    $sticy_args = array(
      'posts_per_page' => 20,
      'taxonomy' => 'ouisurf_section',
      'field' => 'slug',
      'term' => 'series',
      'post__in'  => get_option( 'sticky_posts' )

    );

    $sticky_posts = new WP_Query($sticy_args);

    ?>

    <?php if ($sticky_posts->have_posts()): ?>

    <div class="row">
      <hgroup class="col-md-offset-1 col-md-11 col-xs-12 section--sep">
        <h4><?php _e( "Choix de l'équipe", "ouisurf" ) ?></h4>
        <nav id="slider-controls-most-sticky" class="section--sep--menu visible-xs">
            <a href="#" class="slider--arrow--prev"><i class="ion-ios-arrow-left"></i></a>
            <a href="#" class="slider--arrow--next"><i class="ion-ios-arrow-right"></i></a>
        </nav>
      </hgroup>
    </div>

    <div class="row">

      <div class="col-md-offset-1 col-md-11 col-xs-12 no-padding--right--min-sm">

        <!-- recents posts -->
        <div data-slider-default data-slides-gutter-before="0" data-slider-arrows-container="#slider-controls-most-sticky" data-slides-per-view="3.25" data-slides-per-view-mobile="1.11" class="swiper-container slider--default serie--posts-slider">
          <a href="#" class="slider--arrow--prev in-circle hidden-xs"><i class="ion-ios-arrow-left"></i></a>
					<a href="#" class="slider--arrow--next in-circle hidden-xs"><i class="ion-ios-arrow-right"></i></a>
          <div class="swiper-wrapper">
            <?php while ($sticky_posts->have_posts()) : $sticky_posts->the_post(); ?>
              <div class="swiper-slide">
                <?php get_template_part('templates/compact-video'); ?>
              </div>
            <?php endwhile; ?>
          </div>
        </div>
        <!-- /end recents posts  -->
        <?php wp_reset_postdata(); ?>

      </div>

    </div>
    <!-- /end .row -->

    <?php endif; ?>

    <!-- all series -->
    <?php get_template_part('templates/content-type/series/all-series'); ?>

  </div>

</section>
