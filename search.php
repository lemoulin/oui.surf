<?php

use Roots\Sage\Titles;

?>

<section class="archive-default">

    <header class="archive-header no-featured-posts">
    	<div class="content">
    		<h2 class="archive-title"><?= Titles\title(); ?> <?php get_search_form() ?></h2>        
    	</div>
    </header>

    <div class="container-fluid container-fluid--max-width-xl">
        <div class="row">

            <div class="archive-posts">
                <?php get_template_part('templates/partials', 'no-results'); ?>

                <?php while (have_posts()) : the_post(); ?>

                    <?php get_template_part('templates/compact', ouisurf_post_get_section($post)); ?>

                <?php endwhile; ?>

				<?php the_posts_navigation(array("prev_text" => 'Page suivante <i class="ion-ios-arrow-right"></i>', "next_text" => '<i class="ion-ios-arrow-left"></i> Page Précédente')) ?>

            </div>

            <aside class="archive-sidebar" data-sticky>
                <?php dynamic_sidebar('sidebar-primary'); ?>
				<?php get_template_part("templates/partials/sidebar-sticky-posts") ?>
            </aside>

        </div>
    </div>
    <!-- /.container  -->

</section>
