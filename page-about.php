<?php
/**
* Template Name: À propos
*/
?>

<?php while (have_posts()) : the_post(); ?>

    <article <?php post_class(); ?> data-controller="Post" data-action="pages" data-ouisurf-section="<?php echo $post->slug ?>">

        <?php get_template_part('templates/post-formats/post-header'); ?>

        <div id="entry-content" class="entry-content--pages">

            <!-- entry main content -->
            <div class="container">

                <div class="row">

                    <aside class="col-sm-2">
                        <?php get_template_part('templates/entry-meta'); ?>
                    </aside>

                    <div class="col-sm-8 entry-body">
                        <?php the_content(); ?>
                    </div>

                    <aside class="col-sm-2 sidebar-nav" data-sticky>
                        <?php
                        if (has_nav_menu('about_nav')) :
                            wp_nav_menu(['theme_location' => 'about_nav']);
                        endif;
                        ?>
                    </aside>

                </div>

            </div>
            <!-- /.container -->

        </div>

    </article>


<?php endwhile; ?>
