<?php
use Roots\Sage\Assets;

/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/post-types.php', // Theme Post Types
  'lib/post-formats.php', // Theme Post Formats
  'lib/taxonomies.php',  // Theme Taxonomies
  'lib/customizer.php', // Theme customizer
  'lib/post-functions.php', // Theme custom post functions
  'lib/shortcodes.php', // Theme custom shortcodes
  'lib/hm-core.functions.php' // Humand Made Custom Functions
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);


// Add custom CSS in WP admin
function ouisurf_admin_style() {
    $theme = wp_get_theme();
    $version = $theme->get("Version");
    wp_register_style('admin-styles', Assets\asset_path("$version/css/admin.css"), true, 1);
    wp_enqueue_style( 'admin-styles' );
}

add_action('admin_enqueue_scripts', 'ouisurf_admin_style');



// ACF

function my_acf_flexible_content_layout_title( $title, $field, $layout, $i ) {

  // remove layout title from text
  $title = 'Rangée';

  // get properties
  $fluid = get_sub_field('container_fluid') ? "pleine largeur" : "centrée";
  $padding = get_sub_field('no_padding') ? "sans padding" : "avec padding";
  $bg_color = get_sub_field('container_background_color') ? "avec couleur de fond " . get_sub_field('container_background_color') : "transparente";
  $number_of_cols = count(get_sub_field('longform_column'));

  // return title
  if (get_sub_field('is_text_row')) {
      return "<span class='row-title'>{$title} : <em style='font-weight: normal;'>Colonne de texte standard ({$number_of_cols} colonnes de contenu)</em></span>";
  } else {
      return "<span class='row-title'>{$title} : <em style='font-weight: normal;'>{$fluid}, {$padding}, {$bg_color} ({$number_of_cols} colonnes de contenu)</em></span>";
  }


}

add_filter('acf/fields/flexible_content/layout_title/name=longform_row', 'my_acf_flexible_content_layout_title', 10, 4);
