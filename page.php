<?php

$args = array(
    'sort_order' => 'ASC',
    'sort_column' => 'menu_order',
    'parent' => get_the_id(),
    'child_of' => get_the_id(),
    'post_type' => 'page',
    'post_status' => 'publish'
);

$child_pages = get_pages( $args );

?>

<?php while (have_posts()) : the_post(); ?>

    <article <?php post_class(); ?> data-controller="Post" data-action="pages" data-ouisurf-section="<?php echo $post->slug ?>">

        <?php get_template_part('templates/post-formats/post-header'); ?>

        <div id="entry-content" class="entry-content--pages">

            <!-- entry main content -->
            <div class="container">

                <div class="row">

                    <aside class="col-sm-2">
                        <?php get_template_part('templates/entry-meta'); ?>
                    </aside>

                    <div class="col-sm-8 entry-body">
                        <?php the_content(); ?>
                    </div>

                </div>
                <!-- /.container -->

            </div>

        </article>


    <?php endwhile; ?>
