<?php
/**
* Dossier post
*/
?>

<?php get_template_part('templates/post-formats/post-header', ouisurf_post_get_section($post)); ?>

<div id="entry-content" class="entry-content--<?php echo ouisurf_post_get_section($post) ?>">

    <!-- entry main content -->
    <div class="container">

        <div class="row">

            <aside class="col-sm-2">
              <?php get_template_part('templates/entry-meta'); ?>
            </aside>

            <div class="col-sm-8 entry-body">

                <aside class="ad--bigbox pull-right hidden-xs">
                    <?php dynamic_sidebar('bigbox'); ?>
                </aside>

                <h4 class="entry-content--category"><?php the_category( ' - ' ) ?></h4>

                <?php the_content(); ?>

            </div>

        </div>

    </div>
    <!-- /.container -->

    <!-- long form content -->
    <?php get_template_part('templates/longform/longform-rows'); ?>

</div>
