<?php

use Roots\Sage\Assets;

?>

<footer id="site-footer" class="content-info">
    <div class="container-fluid container-fluid--max-width-xl">

        <div class="-row">

            <aside class="col-sm-1 col-xs-12 footer--logo">
                <img src="<?php Assets\get_asset_path('img/footer/ouisurf-logo-circle-inverted.png'); ?>" class="img-responsive" alt="<?php bloginfo('title') ?>" />
                <hr class="visible-xs">
            </aside>

            <aside class="col-sm-2 col-xs-12">
                <?php
                if (has_nav_menu('social_nav')) :
                  wp_nav_menu(['theme_location' => 'footer_nav']);
                endif;
                ?>
                <hr class="visible-xs">
            </aside>

            <aside class="col-sm-4 col-xs-12">
                <?php dynamic_sidebar('sidebar-footer-newsletter'); ?>
                <hr class="visible-xs">
            </aside>

            <aside class="col-sm-3 col-xs-12">
                <h3><?php _e( "Nous suivre", "ouisurf" ) ?> : </h3>
                <?php
                if (has_nav_menu('social_nav')) :
                  wp_nav_menu(['theme_location' => 'social_nav']);
                endif;
                ?>
                <hr class="visible-xs">
                <footer class="container-fluid copyrights">
                    <div class="row">
                        <p>
                            <?php _e( "© 2010-2018 OuiSurf.<br>Tous droits réservés.", "ouisurf" ) ?>
                        </p>
                        <p>
                            <a href="http://fondsbell.ca/" target="_blank"><img src="<?php Assets\get_asset_path('img/footer/logo-fonds-bell.png'); ?>" width="40" class="logo pull-left" alt="Fonds Bell" /></a>
                            <?php _e( "Produit grâce à la<br>participation du Fonds Bell", "ouisurf" ) ?>
                        </p>
                    </div>
                </footer>
                <hr class="visible-xs">
            </aside>

            <aside class="col-sm-1 col-xs-12 footer-partners">
                <?php if ( have_rows('partner_logos', 'options') ): ?>
                    <ul class="list-unstyled">
                    <?php while ( have_rows('partner_logos', 'options') ) : the_row(); ?>
                        <li><a href="<?php echo the_sub_field('partner_logo_url'); ?>" target="_blank"><img src="<?php echo the_sub_field('partner_logo_image'); ?>" alt="" class="img-responsive" /></a></li>
                    <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
            </aside>

        </div>

    </div>
</footer>


<!-- facebook -->
<div id="fb-root"></div>
<script>
function deferFB() {
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/<?php echo get_locale() ?>/sdk.js#xfbml=1&version=v2.8&appId=290125164442123";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
}

</script>
