<?php

/**
* Video player partial
*/

$post = isset($template_args['post']) ? $template_args['post'] : $post;

// get serie data
$serie          = ouisurf_post_get_serie( $post );
$episode_number = get_field('episode_number', $post->ID);
$vimeo_video_id = get_field("vimeo_video_id", $post->ID);
$youtube_video_id = get_field("youtube_video_id", $post->ID);

?>

<div class="video-player bg-cover b-lazy" data-src="<?php echo ouisurf_post_get_post_thumbnail_url( $post->ID, 'large' ); ?>">

    <div class="box--inner">
        <div class="box--content">

            <!-- button -->
            <?php if ($vimeo_video_id || $youtube_video_id): ?>
                <a href="#video-<?php echo $post->ID ?>" data-video-player-trigger class="btn-play--outlined centered"><i class="ion-ios-play"></i></a>
            <?php endif; ?>

            <!-- the content -->
            <hgroup>
                <h1 class="entry-title h2">
                    <a href="<?php echo get_permalink($post->ID) ?>">
                        <?php echo $post->post_title; ?>
                    </a>
                </h1>
                <h6 class="entry-category no-margin">
                    <?php if ($serie): ?>

                        <?php the_terms( $post->ID, 'series', null, ' - ' ) ?>
                        <?php if ($episode_number): ?>
                            - <?php _e('Épisode', 'ouisurf') ?> <?php echo $episode_number ?>
                        <?php endif; ?>

                    <?php else: ?>

                        <?php the_terms( $post->ID, 'videos', null, ' - ' ) ?>

                    <?php endif; ?>
                </h6>
            </hgroup>

            <time class="video--duration">
                <?php echo get_field('duration', $post) ?>
            </time>

            <!-- embeded player -->
            <?php if ($vimeo_video_id): ?>
              <div id="video-<?php echo $post->ID ?>" data-video-id="<?php echo $vimeo_video_id; ?>" class="video-player--embed">
                <div class="content">
                  <a href="#video-<?php echo $first_episode_video_id ?>" class="btn-close-video"><i class="ion-android-close"></i></a>
                  <iframe data-service="vimeo" src="https://player.vimeo.com/video/<?php echo $vimeo_video_id; ?>?color=ffffff&title=0&byline=0&portrait=0&api=1&id=video-<?php echo $post->ID ?>" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
              </div>
            <?php endif; ?>

            <?php if ($youtube_video_id): ?>
              <div id="video-<?php echo $post->ID ?>" data-video-id="<?php echo $youtube_video_id; ?>" class="video-player--embed">
                <div class="content">
                  <a href="#video-<?php echo $first_episode_video_id ?>" class="btn-close-video"><i class="ion-android-close"></i></a>
                  <iframe data-service="youtube" src="https://www.youtube.com/embed/<?php echo $youtube_video_id; ?>?api=1" width="100%" height="500" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
              </div>
            <?php endif; ?>

        </div>
    </div>

    <img src="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'pixel' ); ?>" alt="" class="preload-pixel" />

</div>
<!-- /.video-player -->
