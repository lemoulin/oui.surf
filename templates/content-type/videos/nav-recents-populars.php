<?php

// get current taxonomy query objects
$taxonomy = get_queried_object();

// print_r($taxonomy);

/**
* Recents and popular post tabs nav
*/

$this_post_ID = isset($post) ? $post->ID : null;

// if in taxonomy archive
if ($taxonomy->taxonomy) {
    $tax_query = array(
        // 'relation' => 'AND',
        // array(
        //     'taxonomy'  => 'series',
        //     'field'     => 'id',
        //     'terms'     => get_terms(array('taxonomy' => 'series', 'fields' => 'ids')),
        //     'operator'  => 'NOT IN'
        // ),
        array(
            'taxonomy'  => $taxonomy->taxonomy,
            'field'     => 'slug',
            'terms'     => $taxonomy->slug
        )
    );
// elsewhere, get all post
} else {
    $tax_query = array(
        array(
            'taxonomy'  => 'ouisurf_section',
            'field'     => 'slug',
            'terms'     => 'videos'
        )
    );
}


$recent_posts_args = array(
    'posts_per_page' => 10,
    'tax_query'	=> $tax_query,
    'orderby' => 'date',
    'order' => 'DESC'
);

$popular_posts_args = array(
    'posts_per_page' => 25,
    'meta_key' => 'post_views_count',
    'orderby' => 'meta_value_num',
    'order' => 'DESC',
    'post_status'  => 'publish',
    'date_query' => array(
        array(
            'after' => '18 months ago'
        )
    ),
    'tax_query'	=> $tax_query
);

$recent_posts = new WP_Query($recent_posts_args);
$popular_posts = new WP_Query($popular_posts_args);

?>


<nav class="col-sm-4 posts-navigator">

    <header class="posts-navigator--header">
        <nav class="tabs">
            <ul>
                <li>
                    <a href="#tab-content--recent-posts">
                        <h5 class="no-margin--top"><?php _e('Plus récentes', 'ouisurf') ?></h5>
                    </a>
                </li>
                <li>
                    <a href="#tab-content--popular-posts">
                        <h5 class="no-margin--top"><?php _e('Plus populaires', 'ouisurf') ?></h5>
                    </a>
                </li>
            </ul>
        </nav>
    </header>

    <div id="tab-content--recent-posts" class="tab-content">

        <ul class="list-unstyled posts-navigator--list">
            <?php while ($recent_posts->have_posts()) : $recent_posts->the_post(); ?>
                <li <?php if ($post->ID == $this_post_ID): ?>class="current-post"<?php endif; ?>>
                    <?php get_template_part('templates/list', get_post_format()); ?>
                </li>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        </ul>

    </div>

    <div id="tab-content--popular-posts" class="tab-content">

        <ul id="tab-content--popular-posts" class="list-unstyled posts-navigator--list">
            <?php while ($popular_posts->have_posts()) : $popular_posts->the_post(); ?>
                <li <?php if ($post->ID == $this_post_ID): ?>class="current-post"<?php endif; ?>>
                    <?php get_template_part('templates/list', get_post_format()); ?>
                </li>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        </ul>

    </div>

</nav>
