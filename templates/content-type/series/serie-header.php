<?php

$taxonomy_id = get_queried_object_id();
$category_url = get_term_link($taxonomy_id, 'ouisurf_section');
$acf_query_selector = "series_$taxonomy_id";
$header_image = get_field('main_image', $acf_query_selector);
$video_header_background = get_field('video_header_background', $acf_query_selector);
$subtitle = get_field('serie_subtitle', $acf_query_selector);
$synopsis = get_field('serie_synopsis', $acf_query_selector);
$date = get_field('serie_date', $acf_query_selector);
$author = get_field('serie_author', $acf_query_selector);
$serie_number = get_field('serie_number', $acf_query_selector);
$dossier = get_field('dossier_associated', $acf_query_selector);
$vimeo_video_id = get_field('vimeo_video_id', $acf_query_selector);
$youtube_video_id = get_field('youtube_video_id', $acf_query_selector);
$sponsor = get_field('sponsor', $acf_query_selector);
$sponsor_url = get_field('sponsor_url', $acf_query_selector);
$serie_logo = get_field('serie_logo', $acf_query_selector);
$have_tracks = false;

// first post
$first_episode = isset( $wp_query->posts[0] ) ? $wp_query->posts[0] : null;
$second_episode = isset( $wp_query->posts[1] ) ? $wp_query->posts[1] : null;

if ($first_episode) {
  $first_episode_vimeo_video_id = get_field('vimeo_video_id', $first_episode->ID);
  $first_episode_youtube_video_id = get_field('youtube_video_id', $first_episode->ID);
}

?>

<header class="serie--header b-lazy bg-cover" <?php if ($header_image): ?>data-src="<?php echo $header_image['sizes']['largest'] ?>"<?php endif; ?> <?php if ($video_header_background): ?>data-video-bg="<?php echo $video_header_background ?>"<?php endif; ?> >

    <?php if ($first_episode_vimeo_video_id): ?>
        <a href="#video-<?php echo $first_episode_vimeo_video_id ?>" data-video-player-trigger data-video-player-hide-navigation="true" class="btn-play centered"><i class="ion-ios-play"></i></a>
    <?php endif; ?>

    <?php if ($first_episode_youtube_video_id): ?>
        <a href="#video-<?php echo $first_episode_youtube_video_id ?>" data-video-player-trigger data-video-player-hide-navigation="true" class="btn-play centered"><i class="ion-ios-play"></i></a>
    <?php endif; ?>

    <hgroup class="container-fluid">

        <div class="row">
            <?php if ($serie_logo): ?>
                <h2 class="h1 col-md-offset-1 col-sm-11 col-xs-12"><img src="<?php echo $serie_logo ?>" alt="<?php echo $category_name ?>" class='serie--header--serie-logo' /></h2>
            <?php else: ?>
                <h2 class="h1 col-md-offset-1 col-sm-11 col-xs-12"><?php echo single_cat_title() ?></h2>
            <?php endif; ?>
        </div>

        <?php if ($sponsor): ?>
            <a href="<?php echo $sponsor_url ?>" target="_blank" class="serie--header--sponsor-logo hidden-xs"><img src="<?php echo $sponsor ?>" alt="<?php _e('Commanditaire', 'ouisurf') ?>" /></a>
        <?php endif; ?>

        <nav class="row">
            <aside class="col-md-offset-1 col-md-11">
                <h6 class="inline-block--margin-right"><?php echo $serie_number ?></h6>
                <?php if ($vimeo_video_id): ?>
                    <a href="#video-<?php echo $vimeo_video_id ?>" data-video-player-trigger data-video-player-hide-navigation="true" class="btn btn-outlined-inverted inline-block--margin-right"><?php _e("Voir la bande annonce", "ouisurf") ?></a>
                <?php endif; ?>
                <?php if ($youtube_video_id): ?>
                    <a href="#video-<?php echo $youtube_video_id ?>" data-video-player-trigger data-video-player-hide-navigation="true" class="btn btn-outlined-inverted inline-block--margin-right"><?php _e("Voir la bande annonce", "ouisurf") ?></a>
                <?php endif; ?>
                <!-- slide down to episode, mobile only -->
                <a href="#episodes-list" class="btn btn-outlined-inverted inline-block--margin-right visible-xs-inline" data-scroll-to><?php _e('Épisodes', 'ouisurf') ?></a>
                <?php if ($dossier): ?>
                    <a href="<?php echo get_permalink($dossier->ID) ?>" class="btn btn-outlined-inverted inline-block--margin-right"><?php _e('Voir le dossier', 'ouisurf') ?></a>
                <?php endif; ?>
                <!-- tracks -->
                <a href="#post-tracks" class="btn btn-outlined-inverted inline-block--margin-right hide" id="btn-tracks" data-slide-toggle><?php _e('Musique de la série', 'ouisurf') ?> <i class="ion-android-arrow-dropdown chevron"></i></a>
            </aside>

        </nav>

    </hgroup>

    <?php if ($vimeo_video_id): ?>
      <div id="video-<?php echo $vimeo_video_id ?>" data-video-id="<?php echo $vimeo_video_id; ?>" class="video-player--embed">
        <div class="content">
  	      <a href="#video-<?php echo $vimeo_video_id ?>" class="btn-close-video"><i class="ion-android-close"></i></a>
  	      <iframe id="video-<?php echo $vimeo_video_id ?>" src="https://player.vimeo.com/video/<?php echo $vimeo_video_id; ?>?color=ffffff&title=0&byline=0&portrait=0&api=1&id=video-<?php echo $vimeo_video_id ?>" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($youtube_video_id): ?>
      <div id="video-<?php echo $youtube_video_id ?>" data-video-id="<?php echo $youtube_video_id; ?>" class="video-player--embed">
        <div class="content">
          <a href="#video-<?php echo $youtube_video_id ?>" class="btn-close-video"><i class="ion-android-close"></i></a>
          <iframe data-service="youtube" src="https://www.youtube.com/embed/<?php echo $youtube_video_id; ?>?api=1" width="100%" height="500" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($first_episode_vimeo_video_id): ?>
      <div id="video-<?php echo $first_episode_vimeo_video_id ?>" data-video-id="<?php echo $first_episode_vimeo_video_id; ?>" class="video-player--embed">
        <div class="content">
  	      <a href="#video-<?php echo $first_episode_vimeo_video_id ?>" class="btn-close-video"><i class="ion-android-close"></i></a>
  	      <iframe id="video-<?php echo $first_episode_vimeo_video_id ?>" src="https://player.vimeo.com/video/<?php echo $vimeo_video_id; ?>?color=ffffff&title=0&byline=0&portrait=0&api=1&id=video-<?php echo $vimeo_video_id ?>" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($first_episode_youtube_video_id): ?>
      <div id="video-<?php echo $first_episode_youtube_video_id ?>" data-video-id="<?php echo $first_episode_youtube_video_id; ?>" class="video-player--embed">
        <div class="content">
          <a href="#video-<?php echo $first_episode_youtube_video_id ?>" class="btn-close-video"><i class="ion-android-close"></i></a>
          <iframe data-service="youtube" src="https://www.youtube.com/embed/<?php echo $first_episode_youtube_video_id; ?>?api=1" width="100%" height="500" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
      </div>
    <?php endif; ?>

    <nav class="adjacent-posts-navigation">
        <?php if ($second_episode): ?>
            <?php $episode_number_next = intval(get_field('episode_number', $second_episode->ID)) ?>
            <a href="<?php echo get_permalink($second_episode->ID) ?>" class="next"><span class="adjacent-posts-navigation--circle">Ep. <?php echo $episode_number_next ?></span><i class="ion-ios-arrow-right"></i></a>
        <?php endif; ?>
    </nav>

</header>

<!-- tracks -->
<?php if (have_posts()) : ?>

<div id="post-tracks" class="post-tracks">

    <a href="#post-tracks" class="post-tracks--close" data-slide-toggle><i class="ion-android-close"></i></a>

    <div class="container">
        <ul class="row list-unstyled">
            <li class="labels row">
                <h5 class="col-sm-3 hidden-xs"><?php _e("Artiste", "ouisurf") ?></h5>
                <h5 class="col-sm-8 hidden-xs"><?php _e("Titre", "ouisurf") ?></h5>
                <h5 class="col-sm-1 text-center no-padding--right hidden-xs"><?php __("Écouter", "ouisurf") ?></h5>
            </li>
            <?php while (have_posts()) : the_post(); ?>
            <?php if (have_rows('tracks', $post)): ?>
            <?php $have_tracks = true; ?>
            <?php while ( have_rows('tracks', $post) ) : the_row(); ?>
            <li class="row">
                <h4 class="col-sm-3"><?php the_sub_field('track_artist', $post) ?></h4>
                <h4 class="col-sm-8"><?php the_sub_field('track_title', $post) ?></h4>
                <?php if (get_sub_field('track_url', $post)): ?>
                <div class="col-sm-1 relative-inner text-center no-padding--right">
                    <a href="<?php the_sub_field('track_url', $post) ?>" class="post-tracks--link" target="_blank"><i class="ion-plus"></i></a>
                </div>
                <?php endif; ?>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php endwhile; ?>

        </ul>
    </div>
</div>

<?php if ($have_tracks): ?>
<style media="screen">
    /* HACK ! HACK ! WACK ! ;*/
    /* force btn track visibility */
    #btn-tracks {
      display: inline-block !important;
    }
</style>
<?php endif; ?>

<?php endif; ?>

<header class="serie--intro">

    <div class="container-fluid ">

        <div class="row">

            <!-- posts -->
            <?php if (have_posts()) : ?>
            <header class="col-sm-10 col-md-12 -no-padding--min-sm">
              <div id="episodes-list" data-slider-default data-slides-gutter-before="0" data-slides-per-view="2.5" data-slides-per-view-mobile="1.05" class="swiper-container slider--default serie--intro--episodes">

                <a href="#" class="slider--arrow--prev in-circle hidden-xs"><i class="ion-ios-arrow-left"></i></a>
                <a href="#" class="slider--arrow--next in-circle hidden-xs"><i class="ion-ios-arrow-right"></i></a>
                <div class="slider--shade right hidden-xs"></div>

                <div class="swiper-wrapper">
                  <?php while (have_posts()) : the_post(); ?>
                    <div class="swiper-slide">
                      <?php get_template_part('templates/compact-serie-preview'); ?>
                    </div>
                  <?php endwhile; ?>
                </div>

                <nav class="slider--pagination swiper-pagination inverted"></nav>

              </div>
            </header>
            <?php endif; ?>
            <!-- end posts  -->

        </div>

        <div class="row">

            <aside class="col-md-offset-1 col-sm-2">

                <div class="entry-meta">

                <?php if ($date): ?>
                    <time><?php echo $date ?></time>
                <?php endif; ?>
                <?php if ($author): ?>
                    <h6 class="post-author">
                        <a href="<?php echo get_author_posts_url($author['ID'], $author['user_nicename']) ?>"><?php echo $author['display_name'] ?></a>
                    </h6>
                <?php endif; ?>

                <?php hm_get_template_part('templates/partials/share-widgets', array('url' => false, 'title' => false, 'classname' => 'inverted')); ?>

                </div>

            </aside>

            <?php if ($sponsor): ?>
            <header class="serie-sponsor--mobile col-xs-12 visible-xs">
                <a href="<?php echo $sponsor_url ?>" target="_blank"><img src="<?php echo $sponsor ?>" alt="Commanditaire" /></a>
            </header>
            <?php endif; ?>

            <div class="col-sm-10 col-md-9 no-padding--min-sm">

                <!-- description with bigbox -->
                <div class="clearfix entry-content">

                    <aside class="col-sm-8 no-padding serie--intro--description entry-body">

                        <aside class="ad--bigbox pull-right hidden-xs">
                            <?php dynamic_sidebar('bigbox'); ?>
                        </aside>

                        <h5 class="no-margin--top uppercase">Séries OuiSurf</h5>
                        <?php if ($subtitle): ?>
                            <h3 class="no-margin--top"><?php echo $subtitle ?></h3>
                        <?php endif; ?>
                        <!-- long synopsis description or archive description -->
                        <?php if ($synopsis): ?>
                            <?php echo $synopsis ?>
                        <?php else: ?>
                            <?php the_archive_description() ?>
                        <?php endif; ?>

                    </aside>

                </div>

            </div>

        </div>
        <!-- row -->


    </div>
    <!-- container-fluid -->

</header>
