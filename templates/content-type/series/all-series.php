<?php
/**
 * Menu listing all series
 */
?>

<section class="all-series container-fluid no-padding--max-xs">

	<div class="row">
		<hgroup class="col-md-offset-1 col-md-11 col-xs-12 section--sep">
			<h4><?php _e( "Les Séries", "ouisurf" ) ?></h4>
		</hgroup>
	</div>

	<div class="row">

		<?php
		// get all series
		$args = array(
			'orderby'   => 'term_order',
			'order'     => 'ASC',
			'taxonomy'  => 'series'
		);
		$series_categories  = get_terms($args);

		?>

		<ul class="all-series--list list-unstyled col-md-10 col-md-offset-1">

			<?php foreach ($series_categories as $category):

			// data for each category
			$acf_query_selector = "series_$category->term_id";
			$permalink          = get_term_link($category, 'ouisurf_section');
			$image              = get_field('main_image', $acf_query_selector);
      $active             = get_field('serie_active', $acf_query_selector);
			?>
      <?php if ($active): ?>
			<li class="col-lg-4 col-md-6 col-xs-12">
				<a href="<?php echo $permalink ?>">
					<figure class="col-xs-6 no-padding">
						<?php if ($image): ?>
						<div class="content b-lazy bg-cover" data-src="<?php echo $image['sizes']['medium'] ?>">
							<img src="<?php echo $image['sizes']['pixel'] ?>" alt="" class="preload-pixel" />
						</div>
						<?php endif; ?>
						<div class="no-image"></div>
					</figure>
					<header class="col-xs-6">
						<h4><?php echo $category->name ?></h4>
						<h6 class="no-margin"><?php echo $category->count ?> <?php _e( "épisodes", "ouisurf" ) ?></h6>
					</header>
				</a>
			</li>
      <?php endif; ?>
			<?php endforeach; ?>

		</ul>

	</div>
	<!-- end .row -->

</section>
