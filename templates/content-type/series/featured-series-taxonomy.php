<?php

$args = array(
	'orderby'   => 'term_order',
	'order'     => 'ASC',
	'taxonomy'  => 'series'
);
$series_categories  = get_terms($args);
$sticky_serie       = null;
$acf_query_selector = null;

// try to find a sticky serie
foreach ($series_categories as $category) {
	$acf_query_selector = "series_$category->term_id";
	if ( get_field('sticky_serie', $acf_query_selector) ) {
		$sticky_serie = $category;
		break;
	}
}

if ($sticky_serie) {
  $active = get_field('serie_active', $acf_query_selector);
	$header_image = get_field('main_image', $acf_query_selector);
	$video_header_background = get_field('video_header_background', $acf_query_selector);
	$serie_number = get_field('serie_number', $acf_query_selector);
	$serie_logo = get_field('serie_logo', $acf_query_selector);
	$vimeo_video_id = get_field('vimeo_video_id', $acf_query_selector);
  $youtube_video_id = get_field('youtube_video_id', $acf_query_selector);
	$category_name = $sticky_serie->name;

	// determine header className
	$header_classname = $header_image ? 'with-background b-lazy bg-cover' : 'no-image';

}

?>

<!-- Most recent Sticky category -->
<?php if ($sticky_serie): ?>
<header 
	-data-href="<?php echo get_term_link($sticky_serie, 'ouisurf_section') ?>"
	class="serie--header <?php echo $header_classname ?>"
	<?php if ($header_image): ?>data-src="<?php echo $header_image['sizes']['largest'] ?>"<?php endif; ?>
	<?php if ($video_header_background): ?>data-video-bg="<?php echo $video_header_background ?>"<?php endif; ?>
>

	<hgroup class="container-fluid">

			<div class="row">
				<?php if ($serie_logo): ?>
					<h2 class="h1 col-md-offset-1 col-sm-11 col-xs-12"><a href="<?php echo get_term_link($sticky_serie, 'ouisurf_section') ?>"><img src="<?php echo $serie_logo ?>" alt="<?php echo $category_name ?>" class='serie--header--serie-logo' /></a></h2>
				<?php else: ?>
					<h2 class="h1 col-md-offset-1 col-sm-11 col-xs-12"><a href="<?php echo get_term_link($sticky_serie, 'ouisurf_section') ?>"><?php echo $category_name ?></a></h2>
				<?php endif; ?>

			</div>

			<nav class="row">
				<aside class="col-md-offset-1 col-md-11">
					<h6 class="inline-block--margin-right"><?php echo $serie_number ?></h6>
					<?php if ($vimeo_video_id): ?>
					<a href="#video-<?php echo $vimeo_video_id ?>" data-video-player-trigger data-video-player-hide-navigation="true" class="btn btn-outlined-inverted inline-block--margin-right"><?php _e("Voir la bande annonce", "ouisurf") ?></a>
					<?php endif; ?>
          <?php if ($youtube_video_id): ?>
					<a href="#video-<?php echo $youtube_video_id ?>" data-video-player-trigger data-video-player-hide-navigation="true" class="btn btn-outlined-inverted inline-block--margin-right"><?php _e( "Voir la bande annonce", "ouisurf" ) ?></a>
					<?php endif; ?>
					<a href="<?php echo get_term_link($sticky_serie, 'ouisurf_section') ?>" class="btn btn-outlined-inverted inline-block--margin-right"><?php _e( "Consulter la série", "ouisurf" ) ?></a>
				</aside>

			</nav>

	</hgroup>

  <!-- embeded player -->
  <?php if ($vimeo_video_id): ?>
    <div id="video-<?php echo $vimeo_video_id ?>" data-video-id="<?php echo $vimeo_video_id; ?>" class="video-player--embed">
      <div class="content">
	      <a href="#video-<?php echo $first_episode_video_id ?>" class="btn-close-video"><i class="ion-android-close"></i></a>
	      <iframe id="video-<?php echo $vimeo_video_id ?>" src="https://player.vimeo.com/video/<?php echo $vimeo_video_id; ?>?color=ffffff&title=0&byline=0&portrait=0&api=1&id=video-<?php echo $vimeo_video_id ?>" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
      </div>
    </div>
  <?php endif; ?>

  <?php if ($youtube_video_id): ?>
    <div id="video-<?php echo $youtube_video_id ?>" data-video-id="<?php echo $youtube_video_id; ?>" class="video-player--embed">
      <div class="content">
        <a href="#video-<?php echo $first_episode_video_id ?>" class="btn-close-video"><i class="ion-android-close"></i></a>
        <iframe data-service="youtube" src="https://www.youtube.com/embed/<?php echo $youtube_video_id; ?>?api=1" width="100%" height="500" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      </div>
    </div>
  <?php endif; ?>

</header>
<?php endif; ?>

<!-- slider of all series -->
<header class="sticky-series--list">

	<div class="container-fluid">

		<div class="row">
			<hgroup class="col-md-offset-1 col-md-11 col-xs-12">
				<h4><?php _e( "Dernières séries OuiSurf", "ouisurf" ) ?></h4>
			</hgroup>
		</div>

		<div class="row">

			<aside class="col-md-offset-1 col-md-11 col-xs-12 no-padding">

				<div data-slider-default data-slides-per-view="3.75" data-slides-per-view-mobile="1.25" class="swiper-container slider--default">

					<a href="#" class="slider--arrow--prev in-circle hidden-xs"><i class="ion-ios-arrow-left"></i></a>
					<a href="#" class="slider--arrow--next in-circle hidden-xs"><i class="ion-ios-arrow-right"></i></a>
					<div class="slider--shade right hidden-xs"></div>

					<div class="swiper-wrapper">

						<?php foreach ($series_categories as $category): ?>
						<?php
						// get if it's a sticky Serie
						$acf_query_selector = "series_$category->term_id";
						$is_sticky_serie = get_field('sticky_serie', $acf_query_selector);
						$active = get_field('serie_active', $acf_query_selector);
						?>
						<!-- exclude category in header and only sticky series -->
						<?php if ( ($category->term_id != $sticky_serie->term_id)  && $is_sticky_serie && $active ): ?>
							<?php hm_get_template_part('templates/content-type/series/taxonomy-serie-compact', array('category' => $category)); ?>
						<?php endif; ?>
						<?php endforeach; ?>

					</div>

					<nav class="slider--pagination swiper-pagination inverted"></nav>

				</div>

			</aside>

		</div>


</header>
