<?php
// retreive category from args
$category           = $template_args['category'];
$acf_query_selector = "series_$category->term_id";
$header_image       = get_field('main_image', $acf_query_selector);
$serie_number       = get_field('serie_number', $acf_query_selector);
$serie_logo         = get_field('serie_logo', $acf_query_selector);
$permalink          = get_term_link($category, 'ouisurf_section');

?>

<article class="taxonomy-serie--compact post-compact--taxonomy-serie swiper-slide">

	<a href="<?php echo $permalink; ?>">
		<figure class="swiper-lazy bg-cover b-lazy box--portrait" data-src="<?php echo $header_image['sizes']['large']; ?>">

			<?php if ($serie_logo): ?>
			<div class="post-compact--serie-logo">
				<img src="<?php echo $serie_logo ?>" alt="<?php echo $category->name ?>" class="img-responsive" />
			</div>
			<?php endif; ?>

			<div class="box--inner">
				<div class="box--content">
					<header>
						<?php if (!$serie_logo): ?>
						<h3 class="entry-title"><?php echo $category->name ?></h3>
						<?php endif; ?>
						<h6><?php echo $category->count ?> <?php _e( "épisodes", "ouisurf" ) ?></h6>
					</header>
				</div>
			</div>
		</figure>
	</a>

	<div class="entry-summary hide">
		<p>
			<?php echo $category->description; ?>
		</p>
	</div>

</article>
