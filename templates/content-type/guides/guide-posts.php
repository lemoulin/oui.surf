<div class="guide--posts">

    <?php $guide_posts = get_field('guide_posts'); ?>
    <?php $guide_posts_title = get_field('guide_posts_title'); ?>

    <header class="guide--posts--header">
        <h4><?php echo $guide_posts_title ?></h4>
    </header>

    <?php if ($guide_posts): ?>
        <?php foreach( $guide_posts as $post): // variable must be called $post (IMPORTANT) ?>
            <?php setup_postdata($post); ?>
            <?php get_template_part('templates/compact-guides'); ?>
        <?php endforeach; ?>
        <?php wp_reset_postdata(); ?>
    <?php endif; ?>

</div>
