<div class="post--longform-content container-fluid">

  <div class="row">

  	<?php while ( have_rows('dossier_content') ) : the_row(); ?>

  		<?php $columns_desktop = get_sub_field('columns_desktop') ?>
  		<?php $columns_mobile = get_sub_field('columns_mobile') ?>
  		<?php $bloc_type = get_sub_field('bloc_type') ?>
  		<?php $title = get_sub_field('title') ?>

  		<aside class="content-type--<?php echo $bloc_type ?> col-md-<?php echo $columns_desktop ?> col-xs-<?php echo $columns_mobile ?>">

        <?php
        /**
         * Text bloc
         */
        ?>
  			<?php if ($bloc_type == 'text'): ?>

  				<?php echo get_sub_field('text_bloc'); ?>

  			<?php endif; ?>


        <?php
        /**
         * Image bloc
         */
        ?>
  			<?php if ($bloc_type == 'image'): ?>

  				<img src="<?php echo get_sub_field('image'); ?>" alt="<?php echo $title ?>" class="img-responsive" />

  			<?php endif; ?>

        <?php
        /**
         * Video background
         */
        ?>
  			<?php if ($bloc_type == 'video'): ?>

  				<video width="100%" height="auto" autoplay muted loop>
  					<source src="<?php echo get_sub_field('background_video_mp4'); ?>" type="video/mp4">
  					<source src="<?php echo get_sub_field('background_video_webm'); ?>" type="video/webm">
  				</video>

  			<?php endif; ?>

        <?php
        /**
         * Video post
         */
        ?>
  			<?php if ($bloc_type == 'video_post'): ?>

  				<?php $video_post = get_sub_field('video_post') ?>
          <?php if (isset( $video_post[0] )): ?>
          <?php hm_get_template_part('templates/content-type/videos/video-player', array('post' => $video_post[0])); ?>
          <?php endif; ?>

  			<?php endif; ?>


        <?php
        /**
         * External embeded video
         */
        ?>
  			<?php if ($bloc_type == 'external_video'): ?>

  				<?php echo get_sub_field('external_video'); ?>

  			<?php endif; ?>

  			<hr>

  		</aside>


  	<?php endwhile; ?>

  </div>
  <!-- /row -->


</div>
<!-- /container-fluid  -->
