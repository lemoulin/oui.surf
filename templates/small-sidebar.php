<?php

$vimeo_video_id = get_field("vimeo_video_id", $post->ID);
$video_duration = get_field('duration', $post->ID);

?>

<article <?php post_class('post-small-sidebar'); ?>>

    <div class="container-fluid">

        <div class="row">

            <aside class="col-xs-5 no-padding--right">
                <a href="<?php the_permalink(); ?>">
                    <figure class="bg-cover b-lazy" data-src="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'video' ); ?>">
                        <?php if ($vimeo_video_id): ?>
                        <i class="ion-ios-play play-icon"></i>
                        <time class="video--duration">
                            <?php echo get_field('duration') ?>
                        </time>
                        <?php endif; ?>
                    </figure>
                </a>
            </aside>

            <aside class="col-xs-7">
                <header>
                    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                </header>
            </aside>

        </div>

    </div>

</article>
