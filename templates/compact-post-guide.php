<article <?php post_class('post-compact--post-guide'); ?>>

    <div class="row">

        <aside class="col-sm-6">
            <a href="<?php the_permalink(); ?>">
                <figure class="bg-cover b-lazy" data-src="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'large' ); ?>">
                    <h5 class="dossier-tag">Dossier</h5>
                </figure>
            </a>
        </aside>

        <aside class="col-sm-6">
            <header>
                <h4 class="entry-category"><?php the_category( ' - ' ) ?></h4>
                <h2 class="entry-title serif"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            </header>
            <div class="entry-summary">
                <?php the_excerpt(); ?>
            </div>
            <footer>
                <?php get_template_part('templates/entry-meta'); ?>
            </footer>
        </aside>

    </div>

</article>
