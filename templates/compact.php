<?php //$post = setup_postdata($post) ?>
<article <?php post_class('post-compact'); ?>>

    <div class="row">

        <aside class="col-sm-5">
            <a href="<?php the_permalink(); ?>">
                <figure class="bg-cover b-lazy" data-src="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'large' ); ?>" -data-src-xs="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'medium' ); ?>">
                    <?php echo ouisurf_post_get_section($post, False, '<h5 class="post-section-tag">', '</h5>', false) ?></h5>
                    <img src="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'pixel' ); ?>" alt="" class="preload-pixel" />
                </figure>
            </a>
        </aside>

        <aside class="col-sm-7">
            <header>
                <h4 class="entry-category"><?php the_category( ' - ' ) ?></h4>
                <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            </header>
            <div class="entry-summary">
                <?php the_excerpt($post->ID); ?>
            </div>
            <footer>
                <?php get_template_part('templates/entry-meta'); ?>
            </footer>
        </aside>

    </div>

</article>
