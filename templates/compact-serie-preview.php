<?php

/**
* Compact serie post
*/

$summary = isset($template_args['summary']) ? $template_args['summary'] : true;
$episode_number = get_field('episode_number') ? get_field('episode_number', $post->ID) : 0;

// determine className for slide
// set as current if this slide has the same ID as parent template Post->ID
if($post->ID == $this_post_ID) {
  $className = "post-compact--serie current";
} else {
  $className = "post-compact--serie";
}

?>


<article <?php post_class($className); ?>>
    <div class="content">
        <h6 class="episode-number h3"><?php echo $episode_number ?></h6>
        <figure data-href="<?php the_permalink(); ?>" class="swiper-lazy bg-cover b-lazy box--sixteen-nine" data-src="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'large' ); ?>">
            <div class="content">
                <a href="<?php the_permalink(); ?>?autoplay=1" class="btn-play--outlined centered"><i class="ion-ios-play"></i></a>
                <header>
                    <h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <h6 class="entry-category hidden-xs">
                        <?php the_terms( get_the_ID(), 'series', null, ' - ' ) ?>
                        <?php if ($episode_number): ?>
                            - <?php _e('Épisode', 'ouisurf') ?> <?php echo $episode_number ?>
                        <?php endif; ?>
                    </h6>
                    <time class="video--duration">
                        <?php echo get_field('duration') ?>
                    </time>
                </header>
            </div>
        </figure>

        <?php if ($summary): ?>
            <div class="entry-summary hidden-xs">
                <?php the_excerpt(); ?>
            </div>
        <?php endif; ?>

    </div>
</article>
