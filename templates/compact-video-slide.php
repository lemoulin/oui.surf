<?php

/**
* Compact video
*/

$summary = isset($template_args['summary']) ? $template_args['summary'] : false;
$is_serie = ouisurf_is_serie( $post );
$episode_number = get_field('episode_number', $post->ID);

?>

<article <?php post_class('post-compact--video-slide'); ?>>
    <div class="container-fluid">

        <div class="row">

            <aside class="col-xs-12 col-sm-6 col-md-7 col-lg-8 no-padding">
                <?php get_template_part('templates/content-type/videos/video-player'); ?>
            </aside>

            <aside class="col-sm-6 col-md-5 col-lg-4 hidden-xs">
                <header class="entry-meta">

                    <?php if ($is_serie): ?>
                        <h6 class="entry-category">
                            <?php the_terms( get_the_ID(), 'series', null, ' - ' ) ?>
                            <?php if ($episode_number): ?>
                                - <?php _e('Épisode', 'ouisurf') ?> <?php echo $episode_number ?>
                            <?php endif; ?>
                        </h6>
                    <?php else: ?>
                        <h6 class="entry-category"><?php the_terms( get_the_ID(), 'videos', null, ' - ' ) ?></h6>
                    <?php endif; ?>

                    <h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                    <div class="entry-summary">
                        <?php the_excerpt(); ?>
                    </div>

                    <?php hm_get_template_part('templates/partials/share-widgets', array('url' => get_permalink($post), 'title' => get_the_title(), 'classname' => 'inverted horizontal')); ?>

                </header>
            </aside>

        </div>
        <!-- /.row -->


    </div>
</article>
