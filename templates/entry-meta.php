<div class="entry-meta">

	<time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
	<p class="byline author vcard"><span class="by"><?= __('Par', 'ouisurf'); ?></span> <a href="<?= get_author_posts_url($post->post_author) ?>" rel="author" class="fn"><?php echo get_the_author_meta('display_name', $post->post_author); ?></a></p>

  <?php if ( is_single() ): ?>
    <?php hm_get_template_part('templates/partials/share-widgets', array('url' => get_permalink($post), 'title' => get_the_title(), 'classname' => 'inverted')); ?>
  <?php else: ?>
    <p class="shares-count"><span data-shares-count="<?php echo get_permalink($post) ?>"></span> <i class="ion-thumbsup"></i></p>
  <?php endif; ?>

</div>
