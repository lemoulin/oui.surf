<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>

    <!-- Facebook auth -->
    <meta property="fb:pages" content="218859584121" />

    <!-- Hotjar Tracking Code for http://oui.surf -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:296998,hjsv:5};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

    <!-- adsense  -->
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        // init ads unit
        (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-2982431745134313"
        //   enable_page_level_ads: true
        });

        window.HOME_URL = "<?php echo get_option( 'home' ) ?>/";

        $(document).ready(function() {
            // (adsbygoogle = window.adsbygoogle || []).push({})
        });
    </script>

</head>
