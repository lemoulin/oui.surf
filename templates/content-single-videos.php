<?php

/**
* Content Single - video
*/

// get serie category
$serie = ouisurf_post_get_serie( $post );
$section = ouisurf_is_serie( $post ) ? "series" : "videos";

?>


<?php get_template_part('templates/post-formats/post-header', $section); ?>

<div class="entry-content--<?php echo ouisurf_post_get_section($post, true, null, null, true) ?>">

    <!-- entry main content -->
    <div class="container">

        <div class="row">

            <aside class="col-sm-2">
              <?php get_template_part('templates/entry-meta'); ?>
            </aside>

            <div class="col-sm-8 entry-body">

                <aside class="ad--bigbox pull-right hidden-xs">
                    <?php dynamic_sidebar('bigbox'); ?>
                </aside>

                <?php the_content(); ?>

            </div>

        </div>

    </div>
    <!-- /.container -->

</div>

<!-- photo album -->
<?php get_template_part('templates/partials/photos-album'); ?>

<?php
/**
* get recent videos
*/
$recent_posts_args = array(
    'posts_per_page' => 18,
    'tax_query'	=> array(
        array(
            'taxonomy'  => 'ouisurf_section',
            'field'     => 'slug',
            'terms'     => 'videos'
        )
    ),
    // exclude current post
    'post__not_in' => array(get_the_ID())
    // 'orderby' => 'rand',
    // 'order' => 'DESC'
);

$recent_posts = new WP_Query($recent_posts_args);

?>

<!-- most recents and popular posts sliders -->
<div class="container section-video--posts-index">

    <div class="row">
        <hgroup class="col-xs-12 section--sep">
            <h4>Autres vidéos</h4>
        </hgroup>
    </div>

    <div class="row">

        <!-- recents posts -->
        <?php while ($recent_posts->have_posts()) : $recent_posts->the_post(); ?>
            <div class="col-xs-12 col-sm-4">
                <?php get_template_part('templates/compact-video'); ?>
            </div>
        <?php endwhile; ?>

        <!-- /end recents posts  -->
        <?php wp_reset_postdata(); ?>


    </div>
    <!-- /end .row -->

    <?php the_posts_navigation(); ?>

</div>
<!-- end .container -->
