<?php

// get custom fields
$acf_query_selector = "series_$serie->term_id";
$header_image = get_field('main_image', $acf_query_selector);
$serie_number = get_field('serie_number', $acf_query_selector);

?>

<article class="post post-small">

    <a href="<?php echo get_term_link($serie, 'ouisurf_section') ?>">

        <figure class="bg-cover b-lazy" data-src="<?php echo $header_image['sizes']['video']; ?>"></figure>

        <footer >
            <header>
                <h2 class="entry-title"><a href="<?php echo get_category_link($serie->term_id) ?>"><?php echo $serie->name ?></title></a></h2>
                <h6 class="no-margin"><?php echo $serie->count ?> <?php _e('épisodes', 'ouisurf') ?></h6>
            </header>
        </footer>

    </a>

</article>
