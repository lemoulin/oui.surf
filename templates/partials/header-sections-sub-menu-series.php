<?php

/**
* Get recents series
*/

$args = array(
	'orderby'   => 'term_order',
	'order'     => 'ASC',
	'taxonomy'  => 'series',
	'number'    => 5
);
$series_categories  = get_terms($args);

?>

<?php if (count($series_categories) > 0): ?>
<div class="sub-menu series">
	<div class="inner">
		<div class="sub-menu--label">
			<h5 class=""><?php _e( "Récents", "ouisurf" ) ?></h5>
			<a href="<?php echo get_term_link($section->term_id) ?>" class="sub-menu--see-all-link"><?php _e( "Voir toutes les séries", "ouisurf" ) ?> <i class="ion-ios-arrow-right"></i></a>
    </div>
    <ul class="sub-menu--posts">
      <?php foreach ($series_categories as $serie): ?>
      <?php $acf_query_selector = "series_$serie->term_id"; ?>
      <?php $active = get_field('serie_active', $acf_query_selector); ?>
			<?php set_query_var( 'serie', $serie ); ?>
			<?php if ($active): ?>
			<li>
				<?php get_template_part('templates/small-series'); ?>
			</li>
			<?php endif; ?>
			<?php endforeach; ?>
			<?php set_query_var( 'serie', null ); ?>
		</ul>
	</div>
</div>
<?php endif; ?>
