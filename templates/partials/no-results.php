<?php
/**
 * No result found template
 */
?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
	<?php _e('Sorry, no results were found.', 'ouisurf'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>
