<?php

/**
 * Related post
*/

$related_posts = yarpp_get_related( null, $post->ID );

// params
$title = isset($template_args['title']) ? $template_args['title'] : __('Très cool aussi', 'ouisurf');

?>

<?php if ( count($related_posts) > 1 ) : ?>

<div class="container">

	<div class="row">

		<hgroup class="col-xs-12">
			<h4><?php echo $title ?></h4>
		</hgroup>

		<section class="post-related--slider col-xs-12">

			<div data-slider-default data-slides-per-view="3.25" data-slides-per-view-mobile="1.15" class="swiper-container slider--default">
				<div class="swiper-wrapper">
				<?php foreach ($related_posts as $related_post):?>
				<?php $post = $related_post; ?>
				<div class="swiper-slide">
					<?php get_template_part('templates/small'); ?>
				</div>
				<?php endforeach; ?>
			</div>

			<?php wp_reset_postdata(); ?>

		</section>

	</div>


</div>

<?php endif; ?>
