<?php

/**
 * Share widgets
*/

$classname = $template_args['classname'];
$title     = $template_args['title'];
$url       = $template_args['url'];

?>

<nav class="share-widgets <?php echo $classname ?>">
	<h6><?php _e('Partager', 'ouisurf') ?></h6>
	<ul class="list-unstyled list-inline">
		<!-- <li><a <?php if ($url): ?>href="<?php echo $url ?>"<?php endif; ?> <?php if ($title): ?>title="<?php echo $title ?>"<?php endif; ?> class="btn-share--facebook"><i class="ion-social-facebook"></i></a></li> -->
		<!-- <li class="hide"><a <?php if ($url): ?>href="<?php echo $url ?>"<?php endif; ?> <?php if ($title): ?>title="<?php echo $title ?>"<?php endif; ?> class="btn-share--twitter"><i class="ion-social-twitter"></i></a></li> -->
		<!-- <li><a href="#" class="btn-share--forward"><i class="ion-forward"></i></a></li> -->
		<!-- <li> -->
			<!-- <div class="getsocial gs-inline-group" data-url="<?php if ($url): ?><?php echo $url ?><?php endif; ?>"></div> -->
		<!-- </li> -->
    <li>
      <div class="fb-like" data-href="<?php echo $url ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
    </li>
		<!-- <li data-shares-count<?php if ($url): ?>="<?php echo $url ?>"<?php endif; ?> class="shares-count"><i class="ion-heart"></i> <span class="count">0</span></;o> -->
	</ul>
	<!-- <p data-shares-count<?php if ($url): ?>="<?php echo $url ?>"<?php endif; ?> class="shares-count"><i class="ion-heart"></i> <span class="count">0</span></p> -->
</nav>
