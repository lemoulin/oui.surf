<?php
// global $wp_query;


/**
* Get recents post for current section
*/

$section_post_args = array(
	'post_type' => 'post',
	'posts_per_page' => 5,
	'tax_query' => array(
		array(
			'taxonomy'   => 'ouisurf_section',
			'field'      => 'slug',
			'terms'      => $section->slug
		)
	)
);
$section_posts = new WP_Query($section_post_args);

?>

<?php if ($section_posts->post_count > 0): ?>
<div class="sub-menu <?php echo $section->slug ?>">
	<div class="inner">
		<div class="sub-menu--label">
			<h5 class="hidden-xs"><?php _e( "Récents", "ouisurf" ) ?></h5>
			<a href="<?php echo get_term_link($section->term_id) ?>" class="sub-menu--see-all-link"><?php _e("Voir tous les", "ouisurf") ?> <?php echo $section->slug ?> <i class="ion-ios-arrow-right"></i></a>
		</div>
		<ul class="sub-menu--posts">
			<?php while ($section_posts->have_posts()) : $section_posts->the_post(); ?>
				<li>
					<?php get_template_part('templates/small'); ?>
				</li>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</ul>
	</div>
</div>
<?php endif; ?>
