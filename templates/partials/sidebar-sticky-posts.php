<?php

/**
* Get recent sticy posts in Dossiers & Videos
*/

$articles_posts_args = array(
	'posts_per_page' => 3,
	'post_type' => 'post',
	'post__in'  => get_option( 'sticky_posts' ),
	'tax_query' => array(
		array(
			'taxonomy' => 'ouisurf_section',
			'field'    => 'slug',
			'terms'    => array('articles'),
			'operator' => 'IN'
		)
	)
);

$articles_posts = new WP_Query( $articles_posts_args );

$dossiers_posts_args = array(
	'posts_per_page' => 3,
	'post_type' => 'post',
	'post__in'  => get_option( 'sticky_posts' ),
	'tax_query' => array(
		array(
			'taxonomy' => 'ouisurf_section',
			'field'    => 'slug',
			'terms'    => array('dossiers'),
			'operator' => 'IN'
		)
	)
);

$dossiers_posts = new WP_Query( $dossiers_posts_args );

$videos_posts_args = array(
	'posts_per_page' => 3,
	'post_type' => 'post',
	'post__in'  => get_option( 'sticky_posts' ),
	'tax_query' => array(
		array(
			'taxonomy' => 'ouisurf_section',
			'field'    => 'slug',
			'terms'    => array('videos'),
			'operator' => 'IN'
		)
	)
);

$videos_posts = new WP_Query( $videos_posts_args );

?>

<section class="sidebar-widget hidden-xs">

	<header class="widget--header">
		<h3><?php _e( "Incontournables", "ouisurf" ) ?></h3>
	</header>

	<div class="widget--content articles">

		<header class="widget--content--header">
			<h4><?php _e( "Articles", "ouisurf" ) ?></h4>
		</header>

		<?php while ($articles_posts->have_posts()) : $articles_posts->the_post(); ?>
			<?php get_template_part('templates/small-sidebar'); ?>
		<?php endwhile; ?>
		<?php wp_reset_postdata() ?>

	</div>

	<div class="widget--content dossiers">

		<header class="widget--content--header">
			<h4><?php _e( "Dossiers", "ouisurf" ) ?></h4>
		</header>

		<?php while ($dossiers_posts->have_posts()) : $dossiers_posts->the_post(); ?>
			<?php get_template_part('templates/small-sidebar'); ?>
		<?php endwhile; ?>
		<?php wp_reset_postdata() ?>

	</div>

	<div class="widget--content vidéos">

		<header class="widget--content--header">
			<h4><?php _e( "Vidéos", "ouisurf" ) ?></h4>
		</header>

		<?php while ($videos_posts->have_posts()) : $videos_posts->the_post(); ?>
			<?php get_template_part('templates/small-sidebar'); ?>
		<?php endwhile; ?>
		<?php wp_reset_postdata() ?>

	</div>


</section>
