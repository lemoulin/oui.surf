<?php

/**
* Photo album
*/

// params
$rows_query_selector = isset($template_args['rows_query_selector']) ? $template_args['rows_query_selector'] : $post;
$rows = get_field_object('gallery', $rows_query_selector );

// print_r($rows);
$total = count($rows['value']);
$position = 0;

?>

<!-- gallery -->
<?php if (have_rows( 'gallery', $rows_query_selector )): ?>

    <div class="container">
        <div class="row section--sep">
            <h4 class="col-xs-12 -no-padding">Album photos</h4>
            <nav id="slider-photo-album-controls" class="section--sep--menu">
                <a href="#" class="slider--arrow--prev"><i class="ion-ios-arrow-left"></i></a>
                <a href="#" class="slider--arrow--next"><i class="ion-ios-arrow-right"></i></a>
            </nav>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">

            <div data-slider-default data-slides-gutter-before="20" data-click-to-slide='true' data-slider-arrows-container="#slider-photo-album-controls" data-slides-per-view-mobile='auto' class="swiper-container slider--default col-xs-12 no-padding--right--min-sm no-padding--left--max-xs">

                <div class="swiper-wrapper">

                    <?php while ( have_rows( 'gallery', $rows_query_selector ) ) : the_row(); ?>
                        <?php $position = ++$position; ?>

                        <figure class="swiper-slide slider--image">
                            <?php $image = get_sub_field('gallery_image'); ?>
                            <picture>
                                <img src="<?php echo $image['sizes']['largest'] ?>" alt="" class="img-responsive">
                            </picture>
                            <figcaption>
                                <!-- <h6 class="pagination"></h6> -->
                                <ins><?php echo $position ?>/<?php echo $total ?></ins>
                                <?php if ($image['description']): ?>
                                    <p>
                                        <?php echo $image['description'] ?>
                                    </p>
                                <?php endif; ?>
                                <?php if ($image['caption']): ?>
                                    <p>
                                        <?php echo $image['caption'] ?>
                                    </p>
                                <?php endif; ?>
                            </figcaption>
                        </figure>
                    <?php endwhile; ?>

                </div>

                <nav class='swiper-pagination slider--pagination'></nav>

                <!-- <a href="#" class="slider--arrow--prev"><i class="ion-ios-arrow-left"></i></a> -->
                <!-- <a href="#" class="slider--arrow--next"><i class="ion-ios-arrow-right"></i></a> -->

            </div>

        </div>
        <!-- /.row  -->

    </div>
    <!-- /.container-fluid -->

<?php endif; ?>
<!-- end gallery -->
