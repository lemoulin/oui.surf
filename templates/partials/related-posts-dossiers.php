<?php

/**
 * Related post
*/

$related_posts = yarpp_get_related( null, $post->ID );

// params
$title = isset($template_args['title']) ? $template_args['title'] : "Cool aussi";

?>

<?php if ( count($related_posts) > 1 ) : ?>

<div class="row">
	<hgroup class="col-md-offset-1 col-md-11 col-xs-12 section--sep">
		<h4><?php echo $title ?></h4>
	</hgroup>
</div>

<div class="row">

	<ul class="post-related list-unstyled col-md-8 col-centered">

		<?php foreach ($related_posts as $related_post):	?>

		<li class="col-sm-4 col-xs-6">
			<a href="<?php echo get_the_permalink($related_post->ID) ?>">
				<figure class="col-xs-5 no-padding">
					<img src="<?php echo ouisurf_post_get_post_thumbnail_url($related_post->ID, "medium") ?>" alt="" class="img-responsive">
				</figure>
				<header class="col-xs-7">
					<h4><?php echo $related_post->post_title ?></h4>
				</header>
			</a>
		</li>

		<?php wp_reset_postdata(); ?>
		<?php endforeach; ?>

	</ul>

</div>

<?php endif; ?>
