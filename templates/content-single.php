<?php
/**
* Standard article post
*/
?>

<?php get_template_part('templates/post-formats/post-header'); ?>

<div id="entry-content" class="entry-content">

    <!-- entry main content -->
    <div class="container">

        <div class="row">

            <aside class="col-sm-2 col-md-2">
              <?php get_template_part('templates/entry-meta'); ?>
            </aside>

            <div class="col-sm-8 entry-body">

                <aside class="ad--bigbox pull-right hidden-xs">
                    <?php dynamic_sidebar('bigbox'); ?>
                </aside>

                <h4 class="entry-content--category hidden-xs"><?php the_category( ' - ' ) ?></h4>

                <?php the_content(); ?>

            </div>

        </div>

    </div>
    <!-- /.container -->

    <!-- long form content -->
    <?php get_template_part('templates/longform/longform-rows'); ?>

</div>

<?php // post nav for infinite scroll ?>
<div data-infinite-scroll-nav>
    <?php $previous = get_adjacent_post(true, null, true, 'ouisurf_section'); ?>
    <?php if ($previous): ?>
        <a href="<?php echo get_the_permalink($previous) ?>" class="hidden-xs">.</a>
    <?php endif; ?>
</div>
