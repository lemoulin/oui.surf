<?php

/**
 * Featured post
 */

if( !is_front_page() ) {
    $section = ouisurf_post_get_section($post);
} else {
    $section = null;
}


?>

<article data-href="<?php the_permalink(); ?>" <?php post_class('post-featured ' . $section) ?>>
    <?php if (is_home()): ?>
    <?php echo ouisurf_post_get_section($post, False, '<h5 class="post-section-tag">', '</h5>', false) ?></h5>
    <?php endif; ?>
    <header class="post-header">
        <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="bg-filled bg-cover b-lazy" data-src-md="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'large' ); ?>" data-src="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'largest' ); ?>">
        <img src="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'pixel' ); ?>" alt="" class="preload-pixel" />
    </div>
</article>
