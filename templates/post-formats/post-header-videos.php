<?php

/**
* Post header - Section : Videos
* Recents and populars video, not in a Serie taxonomy
*/

$this_post_ID = $post->ID;

?>

<header class="post-header--videos">

  <div class="container">

    <div class="row">

      <!-- video content -->
      <aside class="col-sm-8 player">

        <?php hm_get_template_part('templates/content-type/videos/video-player', array('post' => $post)); ?>

      </aside>

      <!-- Recents and popular videos -->
      <span class="hidden-xs">
          <?php get_template_part('templates/content-type/videos/nav-recents-populars'); ?>
      </span>

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

</header>
