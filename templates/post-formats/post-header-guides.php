<?php
/**
* Standard header
*/

$header_image = ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'largest-2x', False );
$video_header_background = get_field('video_header_background');
$sponsor = get_field('sponsor');
$sponsor_url = get_field('sponsor_url');
$vimeo_video_id = get_field("vimeo_video_id", $post->ID);

// determine header className
$header_classname = $header_image ? 'with-background b-lazy bg-cover' : 'post-header--no-image';

?>

<header class="post-header--guides <?php echo $header_classname ?>" <?php if ($header_image): ?>data-src="<?php echo $header_image ?>"<?php endif; ?> <?php if ($video_header_background): ?>data-video-bg="<?php echo $video_header_background ?>"<?php endif; ?> >

    <?php if ($vimeo_video_id): ?>
    <a href="#video-<?php echo $post->ID ?>" data-video-player-trigger class="btn-play--outlined centered"><i class="ion-ios-play"></i></a>
    <?php endif; ?>

    <hgroup class="container">

        <div class="row">

            <div class="col-sm-10 col-sm-offset-2">

                <!-- main title -->
                <h1 class="entry-title"><?php the_title(); ?></h1>

                <?php if (get_the_subtitle($post, '', '', false)): ?>
                <!-- sub-title -->
                <h2 class="entry-subtitle h4"><?php the_subtitle(); ?></h2>
                <?php endif; ?>

                <h5 class="guide--label">Guide</h5>

            </div>

        </div>

    </hgroup>

    <?php if ($sponsor): ?>
        <div class="sponsor-logo">
            <a href="<?php echo $sponsor_url ?>" target="_blank"><img src="<?php echo $sponsor ?>" alt="Commanditaire" /></a>
        </div>
    <?php endif; ?>

    <?php if ($vimeo_video_id): ?>
      <iframe id="video-<?php echo $post->ID ?>" src="https://player.vimeo.com/video/<?php echo $vimeo_video_id; ?>?color=ffffff&title=0&byline=0&portrait=0&api=1&id=video-<?php echo $post->ID ?>" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    <?php endif; ?>

    <div class="preload-pixel-container">
        <img src="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'pixel' ); ?>" alt="" class="preload-pixel" />
    </div>

</header>
