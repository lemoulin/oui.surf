<?php
/**
* Standard header
*/

$header_image = ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'largest-2x', False );
$video_header_background = get_field('video_header_background');
$sponsor = get_field('sponsor');
$sponsor_url = get_field('sponsor_url');
$vimeo_video_id = get_field("vimeo_video_id", $post->ID);
$youtube_video_id = get_field("youtube_video_id", $post->ID);

// determine header className
$header_classname = $header_image ? 'with-background b-lazy bg-cover' : 'no-image';

?>

<header class="post-header--articles <?php if ($vimeo_video_id || $youtube_video_id): ?>with-embeded-video<?php endif; ?> <?php echo $header_classname ?>" <?php if ($header_image): ?>data-src="<?php echo $header_image ?>"<?php endif; ?> <?php if ($video_header_background): ?>data-video-bg="<?php echo $video_header_background ?>"<?php endif; ?> >

    <?php if ($vimeo_video_id): ?>
    <a href="#video-<?php echo $vimeo_video_id ?>" data-video-player-trigger data-video-player-hide-navigation="true" class="btn-play--outlined centered hidden-xs"><i class="ion-ios-play"></i></a>
    <?php endif; ?>

    <?php if ($youtube_video_id): ?>
    <a href="#video-<?php echo $youtube_video_id ?>" data-video-player-trigger data-video-player-hide-navigation="true" class="btn-play--outlined centered hidden-xs"><i class="ion-ios-play"></i></a>
    <?php endif; ?>

    <hgroup class="container">

        <div class="row">

            <div class="col-sm-10 col-sm-offset-2">

                <!-- main title -->
                <h1 class="entry-title"><?php the_title(); ?></h1>

                <?php if (get_the_subtitle(null, '', '', false)): ?>
                <!-- sub-title -->
                <h2 class="entry-subtitle h4"><?php the_subtitle(); ?></h2>
                <?php endif; ?>

                <h4 class="entry-category visible-xs"><?php the_category( ' - ' ) ?></h4>

            </div>

        </div>

    </hgroup>

    <?php if ($sponsor): ?>
        <div class="sponsor-logo">
            <a href="<?php echo $sponsor_url ?>" target="_blank"><img src="<?php echo $sponsor ?>" alt="Commanditaire" /></a>
        </div>
    <?php endif; ?>

    <?php if ($vimeo_video_id): ?>
      <div id="video-<?php echo $vimeo_video_id ?>" data-video-id="<?php echo $vimeo_video_id; ?>" class="video-player--embed">
        <div class="content">
  	      <a href="#video-<?php echo $vimeo_video_id ?>" class="btn-close-video"><i class="ion-android-close"></i></a>
  	      <iframe id="video-<?php echo $vimeo_video_id ?>" src="https://player.vimeo.com/video/<?php echo $vimeo_video_id; ?>?color=ffffff&title=0&byline=0&portrait=0&api=1&id=video-<?php echo $vimeo_video_id ?>" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($youtube_video_id): ?>
      <div id="video-<?php echo $youtube_video_id ?>" data-video-id="<?php echo $youtube_video_id; ?>" class="video-player--embed">
        <div class="content">
          <a href="#video-<?php echo $youtube_video_id ?>" class="btn-close-video"><i class="ion-android-close"></i></a>
          <iframe data-service="youtube" src="https://www.youtube.com/embed/<?php echo $youtube_video_id; ?>?api=1" width="100%" height="500" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
      </div>
    <?php endif; ?>

    <div class="preload-pixel-container">
        <img src="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'pixel' ); ?>" alt="" class="preload-pixel" />
    </div>

</header>
