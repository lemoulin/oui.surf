<?php

/**
* Post header - Section : Series
*/

$header_image = ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'largest-2x', False );
$video_header_background = get_field('video_header_background');
$sponsor = get_field('sponsor');
$sponsor_url = get_field('sponsor_url');
$vimeo_video_id = get_field("vimeo_video_id", $post->ID);
$youtube_video_id = get_field("youtube_video_id", $post->ID);
$have_tracks = have_rows('tracks');

// determine header className
$header_classname = $header_image ? 'with-background b-lazy bg-cover' : 'no-image';

// get current serie
$serie = ouisurf_post_get_serie( $post, $slug = false );
$this_post_ID = $post->ID;

// custom fields
$episode_number = intval(get_field('episode_number'));

// next and previous posts
$previous_episode = get_adjacent_post(true, null, true, 'series');
$next_episode = get_adjacent_post(true, null, false, 'series');

?>

<header class="post-header--series <?php echo $header_classname ?>" <?php if ($header_image): ?>data-src="<?php echo $header_image ?>"<?php endif; ?> <?php if ($video_header_background): ?>data-video-bg="<?php echo $video_header_background ?>"<?php endif; ?> >


    <?php if ($vimeo_video_id): ?>
    <a href="#video-<?php echo $post->ID ?>" data-video-player-trigger data-video-player-hide-navigation="true" class="btn-play--outlined centered"><i class="ion-ios-play"></i></a>
    <?php endif; ?>

    <?php if ($youtube_video_id): ?>
    <a href="#video-<?php echo $post->ID ?>" data-video-player-trigger data-video-player-hide-navigation="true" class="btn-play--outlined centered"><i class="ion-ios-play"></i></a>
    <?php endif; ?>

    <hgroup class="container">

        <div class="row">

            <div class="col-sm-10 col-sm-offset-2">

                <!-- main title -->
                <h1 class="entry-title"><?php the_title(); ?></h1>
                <h2 class="entry-subtitle h4 inline-block--margin-right"><?php the_terms( $post->ID, 'series', null, ' - ' ) ?> <?php if ($episode_number): ?>- <?php _e('Épisode', 'ouisurf') ?> <?php echo $episode_number ?><?php endif; ?></h2>
                <?php if ($have_tracks): ?>
                <a href="#post-tracks" class="btn btn-outlined-inverted inline-block--margin-right" data-slide-toggle><?php _e('Musique de la série', 'ouisurf') ?>  <i class="ion-android-arrow-dropdown chevron"></i></a>
                <?php endif; ?>
                <?php if (get_the_subtitle(null, '', '', false)): ?>
                <!-- sub-title -->
                <h2 class="entry-subtitle h4"><?php the_subtitle(); ?></h2>
                <?php endif; ?>

            </div>

        </div>

    </hgroup>

    <?php if ($sponsor): ?>
      <div class="sponsor-logo">
        <a href="<?php echo $sponsor_url ?>" target="_blank"><img src="<?php echo $sponsor ?>" alt="Commanditaire" /></a>
      </div>
    <?php endif; ?>

    <?php if ($vimeo_video_id): ?>
      <div id="video-<?php echo $post->ID ?>" data-video-id="<?php echo $vimeo_video_id; ?>" class="video-player--embed">
        <div class="content">
          <a href="#video-<?php echo $first_episode_video_id ?>" class="btn-close-video"><i class="ion-android-close"></i></a>
          <iframe data-service="vimeo" src="https://player.vimeo.com/video/<?php echo $vimeo_video_id; ?>?color=ffffff&title=0&byline=0&portrait=0&api=1&id=video-<?php echo $post->ID ?>" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($youtube_video_id): ?>
      <div id="video-<?php echo $post->ID ?>" data-video-id="<?php echo $youtube_video_id; ?>" class="video-player--embed">
        <div class="content">
          <a href="#video-<?php echo $first_episode_video_id ?>" class="btn-close-video"><i class="ion-android-close"></i></a>
          <iframe data-service="youtube" src="https://www.youtube.com/embed/<?php echo $youtube_video_id; ?>?api=1" width="100%" height="500" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
      </div>
    <?php endif; ?>

    <nav class="adjacent-posts-navigation">
        <?php if ($previous_episode): ?>
            <?php $episode_number_previous = intval(get_field('episode_number', $previous_episode->ID)) ?>
            <a href="<?php echo get_permalink($previous_episode->ID) ?>" class="previous"><i class="ion-ios-arrow-left"></i><span class="adjacent-posts-navigation--circle">Ep. <?php echo $episode_number_previous ?></span></a>
        <?php endif; ?>
        <?php if ($next_episode): ?>
            <?php $episode_number_next = intval(get_field('episode_number', $next_episode->ID)) ?>
            <a href="<?php echo get_permalink($next_episode->ID) ?>" class="next"><span class="adjacent-posts-navigation--circle">Ep. <?php echo $episode_number_next ?></span><i class="ion-ios-arrow-right"></i></a>
        <?php endif; ?>
    </nav>

</header>

<?php if ($have_tracks): ?>
<div id="post-tracks" class="post-tracks">

    <a href="#post-tracks" class="post-tracks--close" data-slide-toggle><i class="ion-android-close"></i></a>

    <div class="container">
        <ul class="row list-unstyled">
            <li class="labels row">
                <h5 class="col-sm-3 hidden-xs"><?php _e('Artiste', 'ouisurf') ?></h5>
                <h5 class="col-sm-8 hidden-xs"><?php _e('Titre', 'ouisurf') ?></h5>
                <h5 class="col-sm-1 text-center no-padding--right hidden-xs"><?php _e('Écouter', 'ouisurf') ?></h5>
            </li>
            <?php $i = 1; ?>
            <?php while ( have_rows('tracks') ) : the_row(); ?>
            <li class="row">
                <h4 class="col-sm-3"><span class="hidden-xs"><?php echo $i; ?> - </span><?php the_sub_field('track_artist') ?></h4>
                <h4 class="col-sm-8"><?php the_sub_field('track_title') ?></h4>
                <?php if (get_sub_field('track_url')): ?>
                <div class="col-sm-1 relative-inner text-center no-padding--right">
                    <a href="<?php the_sub_field('track_url', $post) ?>" class="post-tracks--link" target="_blank"><i class="ion-plus"></i></a>
                </div>
                <?php endif; ?>
            </li>
            <?php $i++; endwhile; ?>
        </ul>
    </div>
</div>
<?php endif; ?>
