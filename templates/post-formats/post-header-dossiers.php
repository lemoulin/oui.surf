<?php
/**
* Dossier header
*/

$header_image = ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'largest-2x' );
$video_header_background = get_field('video_header_background');
$sponsor = get_field('sponsor');
$sponsor_url = get_field('sponsor_url');

// determine header className
$header_classname = $header_image ? 'with-background b-lazy bg-cover' : 'no-image';

?>

<header class="post-header--dossiers <?php echo $header_classname ?>" <?php if ($header_image): ?>data-src="<?php echo $header_image ?>"<?php endif; ?> <?php if ($video_header_background): ?>data-video-bg="<?php echo $video_header_background ?>"<?php endif; ?> >
    <hgroup>

        <h1 class="entry-title"><?php the_title(); ?></h1>
        <?php // XXX : FIX ME ?>
        <?php if (get_the_subtitle($post, '', '', false)): ?>
            <hr>
            <h2 class="entry-subtitle h4"><?php the_subtitle(); ?></h2>
        <?php endif; ?>

        <?php if ($sponsor): ?>
            <div class="sponsor-logo">
                <a href="<?php echo $sponsor_url ?>" target="_blank"><img src="<?php echo $sponsor ?>" alt="Commanditaire" /></a>
            </div>
        <?php endif; ?>

        <?php get_template_part('templates/entry-meta'); ?>

        <a href="#entry-content" data-scroll-to><i class="ion-ios-arrow-down"></i></a>

    </hgroup>

    <div class="preload-pixel-container">
        <img src="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'pixel' ); ?>" alt="" class="preload-pixel" />
    </div>

</header>
