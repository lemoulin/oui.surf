<?php

/**
 * Using same template for as Ouisurf_Sections:Videos
 */

 // get current serie
$serie = ouisurf_post_get_serie( $post, $slug = false );
$this_post_ID = $post->ID;

if ($serie) {
   $args = array(
     'posts_per_page' => -1,
     'taxonomy' => 'series',
     'field' => 'slug',
     'term' => $serie->slug,
     'meta_key' => 'episode_number',
     'orderby' => 'meta_value_num',
     'order' => 'ASC'
   );
   $related_posts = new WP_Query($args);
   // sort($related_posts->posts, SORT_NATURAL);
}

// get current episode number
$episode_number = intval(get_field('episode_number'));

// next episode number

if ($episode_number) {
  $next_episode_number = ($episode_number - 1) + 1;
} else {
  $next_episode_number = 0;
}

?>

<?php get_template_part('templates/post-formats/post-header-series'); ?>

<div class="entry-content--<?php echo ouisurf_post_get_section($post, true, null, null, true) ?>">

    <!-- entry main content -->
    <div class="container">

        <div class="row">

            <aside class="col-sm-2">
              <?php get_template_part('templates/entry-meta'); ?>
            </aside>

            <div class="col-sm-8 entry-body">

                <aside class="ad--bigbox pull-right hidden-xs">
                    <?php dynamic_sidebar('bigbox'); ?>
                </aside>

                <?php the_content(); ?>

            </div>

        </div>

    </div>
    <!-- /.container -->


    <div class="container-fluid">
      <div class="row">
          <!-- posts -->
          <?php if ($related_posts->have_posts()) : ?>
          <header class="col-sm-10 col-md-12">

            <h4><?php _e('Autres épisodes', 'ouisurf') ?></h4>

            <div id="episodes-list" data-slider-default data-slides-gutter-before="0" data-slides-per-view="2.5" data-slides-per-view-mobile="1.05" data-slides-initial-position="<?= $next_episode_number ?>" class="swiper-container slider--default serie--intro--episodes">

              <a href="#" class="slider--arrow--next in-circle hidden-xs"><i class="ion-ios-arrow-right"></i></a>
              <div class="slider--shade right hidden-xs"></div>

              <div class="swiper-wrapper">
                <?php while ($related_posts->have_posts()) : $related_posts->the_post(); ?>
                  <div class="swiper-slide">
                    <?php include(locate_template('templates/compact-serie-preview.php')); ?>
                  </div>
                <?php endwhile; ?>
              </div>

              <nav class="slider--pagination swiper-pagination inverted"></nav>

            </div>
          </header>
          <?php endif; ?>
          <!-- end posts  -->
      </div>
    </div>

</div>

<?php get_template_part('templates/partials/photos-album'); ?>
