<?php

use Roots\Sage\Titles;

/**
 * Header for archives index with 2 most recent posts
 */

global $featured_posts;

 $featured_posts = Array();
 isset($posts[0]) ? $featured_posts[0] = $posts[0] : null;
 isset($posts[1]) ? $featured_posts[1] = $posts[1] : null;

?>


<header class="archive-header">
	<div class="content">
		<h2 class="archive-title"><?= Titles\title(); ?></h2>

		<?php $post = $featured_posts[0] ?>
		<?php get_template_part('templates/featured', ouisurf_post_get_section($post)); ?>

		<?php $post = $featured_posts[1] ?>
		<?php get_template_part('templates/featured', ouisurf_post_get_section($post)); ?>

	</div>
</header>
