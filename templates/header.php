<?php

use Roots\Sage\Assets;

// get all ouisurf_section taxonomies
$args = array(
    'orderby'   => 'term_order',
    'order'     => 'ASC',
    'taxonomy'  => 'ouisurf_section'
);
$sections = get_terms($args);

?>

<header id="site-header" class="banner">
    <div class="container-fluid">

        <div class="row">

            <a href="#main-nav" id="burger-trigger" class="burger visible-xs"></a>

            <hgroup class="logo">
                <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                    <img src="<?php Assets\get_asset_path('svg/ouisurf-logo.svg'); ?>" class="logo inline-svg" alt="<?php bloginfo('title') ?>" />
                </a>
            </hgroup>

            <nav id="main-nav" class="nav-primary">
                <!-- Sections nav -->
                <ul class="menu main-nav--menu">
                    <?php // for each ouisurf_section ?>
                    <?php foreach ($sections as $section): ?>
                    <?php set_query_var( 'section', $section ); ?>
                    <li class="menu-item">
                        <a href="<?php echo get_term_link($section->term_id) ?>" data-slide-toggle-sub-nav class="sub-menu--section--slide-toggle"><?php echo $section->name ?></a>
                        <a href="#<?php echo $section->slug ?>" data-slide-toggle-sub-nav class="sub-menu--slide-toggle visible-xs"><i class="ion-ios-arrow-down"></i></a>
                        <?php get_template_part('templates/partials/header-sections-sub-menu', $section->slug) ?>
                    </li>
                    <?php set_query_var( 'section', null ); ?>
                    <?php endforeach; ?>
                </ul>
                <?php
                if (has_nav_menu('primary_navigation')) :
                    wp_nav_menu(['theme_location' => 'primary_navigation']);
                endif;
                ?>
                <div class="social-nav visible-xs">
                    <h4><?php _e( "Nous suivre", "ouisurf" ) ?> :</h4>
                    <?php
                    if (has_nav_menu('social_nav')) :
                      wp_nav_menu(['theme_location' => 'social_nav']);
                    endif;
                    ?>
                </div>
            </nav>

            <nav class="nav-secondary">
                <?php
                if (has_nav_menu('language_nav')) :
                    wp_nav_menu(['theme_location' => 'language_nav']);
                endif;
                ?>
                <a href="#" class="nav-item--search-btn">
                <img src="<?php Assets\get_asset_path('svg/icn-search.svg'); ?>" class="search-icon inline-svg" alt="<?php _e( "Recherche", "ouisurf" ) ?>" />
            </a>
            </nav>



        </div>

    </div>
</header>

<?php

/**
* Get recent sticy posts in Dossiers & Videos
*/

$dossiers_posts_args = array(
	'posts_per_page' => 3,
	'post_type' => 'post',
	'post__in'  => get_option( 'sticky_posts' ),
	'tax_query' => array(
		array(
			'taxonomy' => 'ouisurf_section',
			'field'    => 'slug',
			'terms'    => array('dossiers'),
			'operator' => 'IN'
		)
	)
);

$dossiers_posts = new WP_Query( $dossiers_posts_args );

$videos_posts_args = array(
	'posts_per_page' => 3,
	'post_type' => 'post',
	'post__in'  => get_option( 'sticky_posts' ),
	'tax_query' => array(
		array(
			'taxonomy' => 'ouisurf_section',
			'field'    => 'slug',
			'terms'    => array('videos'),
			'operator' => 'IN'
		)
	)
);

$videos_posts = new WP_Query( $videos_posts_args );

?>

<nav id="search-bar">

    <div class="container">
        <div class="row">
            <a href="#" class="search-bar--close"><i class="ion-ios-close-empty"></i></a>
            <header class="col-xs-12">
                <?php get_search_form() ?>
            </header>

        </div>

        <footer class="row">
            <?php get_template_part("templates/partials/sidebar-sticky-posts") ?>
        </footer>

    </div>

</nav>
