<?php

$vimeo_video_id = get_field("vimeo_video_id", $post->ID);
$video_duration = get_field('duration', $post->ID);

?>

<article <?php post_class('post-small'); ?>>

    <a href="<?php the_permalink(); ?>">

        <figure class="bg-cover b-lazy" data-src="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'video' ); ?>">

            <?php if ($vimeo_video_id): ?>
            <i class="ion-ios-play play-icon"></i>
            <time class="video--duration">
                <?php echo get_field('duration') ?>
            </time>
            <?php endif; ?>

        </figure>

        <footer>
            <header>
                <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            </header>
        </footer>

    </a>


</article>
