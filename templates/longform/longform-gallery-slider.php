<?php

/**
* Photo album
*/

// params
$medias = $template_args['medias'];
$total = count($medias);
$position = 0;

?>

<!-- gallery -->
<?php if ( $total > 0 ): ?>

    <div data-slider-default data-slides-per-view-mobile='auto' class="swiper-container slider--default">

        <div class="swiper-wrapper">

            <?php foreach ($medias as $media): ?>
                <?php $position = ++$position; ?>

                <figure class="swiper-slide slider--image">
                    <picture>
                        <img src="<?php echo $media['sizes']['large'] ?>" alt="" class="img-responsive">
                    </picture>
                    <figcaption>
                        <ins><?php echo $position ?>/<?php echo $total ?></ins>
                        <?php if ($media['description']): ?>
                            <p><?php echo $media['description'] ?></p>
                        <?php endif; ?>
                        <?php if ($media['caption']): ?>
                            <p><?php echo $media['caption'] ?></p>
                        <?php endif; ?>
                    </figcaption>
                </figure>
            <?php endforeach; ?>

        </div>

        <a href="#" class="slider--arrow--prev"><i class="ion-ios-arrow-left"></i></a>
        <a href="#" class="slider--arrow--next"><i class="ion-ios-arrow-right"></i></a>

    </div>


<?php endif; ?>
<!-- end gallery -->
