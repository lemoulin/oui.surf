<div class="post--longform-content">

    <?php while ( have_rows('longform_row') ) : the_row(); ?>

    <?php

    /**
    * Row fields
    */

    $container_class            = get_sub_field('container_fluid') ? "container-fluid" : "container";
    $is_text_row                = get_sub_field('is_text_row');
    $container_padding          = get_sub_field('no_padding') ? "no-padding" : "";
    $container_background_color = get_sub_field('container_background_color');
    $container_background_image = get_sub_field('background_image');
    $background_image_fixed     = get_sub_field('background_image_fixed') ? "background-fixed" : "";
    $row_cols_margin            = get_sub_field('row_cols_margin') ? "col-with-margin" : "";

    // echo  $is_text_row;


    ?>

    <!-- row container_class begins here -->
    <div class="post--longform-content--row <?php echo $container_padding ?>" <?php if ($container_background_color ): ?>style="background-color: <?php echo $container_background_color ?>"<?php endif; ?>>


    <div class="<?php echo $container_class ?>">

        <?php if ($container_background_image): ?>
            <div class="bg-filled bg-cover b-lazy auto-height <?php echo $background_image_fixed ?>" data-src="<?php echo $container_background_image['url']; ?>">
                <img src="<?php echo $container_background_image['sizes']['pixel']; ?>" alt="" class="preload-pixel" />
            </div>
        <?php endif; ?>

        <!-- row begins -->
        <div class="row">

        <?php // regular text column, matching post top area column system, the offset reprensent post-meta aside element ?>
        <?php if ($is_text_row): ?>
            <div class="col-sm-8 col-sm-offset-2 entry-body">
                <?php echo get_sub_field('row_content_body'); ?>
            </div>
        <?php endif; ?>
        <?php // end regular text column ?>

        <?php // each column ?>
        <?php while ( have_rows('longform_column') ) : the_row(); ?>

        <?php
        /**
        * Get All fields for current column
        */

        $height_ratio             = get_sub_field('height_ratio');
        $columns_desktop          = get_sub_field('columns_desktop');
        $columns_mobile           = get_sub_field('columns_mobile');
        $gutter                   = get_sub_field('gutter');
        $remove_padding           = get_sub_field('remove_padding') ? "no-padding" : "";
        $remove_margin            = get_sub_field('remove_margin');
        $vertical_centered        = get_sub_field('vertical_center_text') ? "vertical-centered" : null;
        $col_centerered           = get_sub_field('col_centerered') ? "col-centered" : null;
        $columns_mobile           = get_sub_field('columns_mobile');
        $bloc_type                = get_sub_field('bloc_type');
        $reveal_transition        = get_sub_field('reveal_transition') ? "wow " . get_sub_field('reveal_transition') : "";
        $reveal_transition_delay  = get_sub_field('reveal_transition_delay') ? "data-wow-duration='1.5s' data-wow-delay='" . get_sub_field('reveal_transition_delay') . "'" : "";



        // content
        $text                      = get_sub_field('text_bloc');
        $image                     = get_sub_field('image');
        $show_image_caption        = get_sub_field('show_image_caption');
        $background_image_fixed    = get_sub_field('background_image_fixed') ? "background-fixed" : "";
        $gallery_slider            = get_sub_field('gallery_slider');
        $background_video          = get_sub_field('background_video');
        $background_video_caption  = get_sub_field('background_video_caption');
        $bloc_height               = get_sub_field('bloc_height') ? "height:". get_sub_field('bloc_height') . "px;" : "";
        $background_color          = get_sub_field('background_color') ? "background-color:". get_sub_field('background_color') . ";" : "";
        $video_posts               = get_sub_field('video_post');

        // col className
        $colClassName   = "longform-bloc content-type--{$bloc_type} col-md-{$columns_desktop} col-xs-{$columns_mobile} {$row_cols_margin} {$height_ratio} {$col_centerered} {$reveal_transition}";

        // style block
        $style_block    = "style='{$background_color} {$bloc_height}'"

        ?>

        <aside <?php echo $style_block ?> class="<?php echo $colClassName ?>" <?php echo $reveal_transition_delay ?>>

            <div class="content <?php echo $vertical_centered ?>">

                <?php if ($bloc_type == 'text' && $text): ?>
                    <div class="post--longform-col--text">
                        <?php echo $text; ?>
                    </div>
                <?php endif; ?>


                <?php if ($bloc_type == 'image'): ?>
                    <div class="post--longform-col--image">
                        <figure>
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $title ?>" class="img-responsive" />
                            <?php $caption = get_post_field('post_excerpt', $image['ID']); ?>
                            <?php if ($caption && $show_image_caption): ?>
                            <figcaption>
                                <?php echo $caption ?>
                            </figcaption>
                            <?php endif; ?>
                        </figure>
                    </div>
                <?php endif; ?>


                <?php if ($bloc_type == 'video' && $background_video): ?>
                    <div class="auto-height" data-video-bg="<?php echo $background_video ?>" <?php if ($image): ?><?php endif; ?> data-video-poster="<?php echo $image['url']; ?>"></div>
                    <?php if ($background_video_caption): ?>
                    <div class="video-bg-caption">
                        <p>
                            <?php echo $background_video_caption ?>
                        </p>
                    </div>
                    <?php endif; ?>
                <?php endif; ?>


                <?php if ($bloc_type == 'video_post'): ?>

                    <?php if ( count ( $video_posts ) == 1): ?>
                        <?php hm_get_template_part('templates/content-type/videos/video-player', array('post' => $video_posts[0])); ?>
                    <?php endif; ?>

                    <?php if ( count ( $video_posts ) > 1): ?>
                        <div data-slider-default data-slides-per-view="1.75" class="swiper-container slider--default">
                            <div class="swiper-wrapper">
                                <?php foreach ($video_posts as $video_post): ?>
                                    <div class="swiper-slide">
                                        <?php hm_get_template_part('templates/content-type/videos/video-player', array('post' => $video_post)); ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                <?php endif; ?>

                <?php if ($bloc_type == 'gallerie'): ?>
                    <?php hm_get_template_part('templates/longform/longform-gallery-slider', array('medias' => $gallery_slider)); ?>
                <?php endif; ?>

                <?php if ($bloc_type == 'background_image' || ( $bloc_type == 'text' && $image ) ): ?>
                    <?php $caption = get_post_field('post_excerpt', $image['ID']); ?>
                    <div class="bg-filled bg-cover b-lazy auto-height <?php echo $background_image_fixed ?>" data-src="<?php echo $image['url']; ?>">
                        <img src="<?php echo $image['sizes']['pixel']; ?>" alt="" class="preload-pixel" />
                    </div>
                    <?php if ($caption && $show_image_caption): ?>
                    <figcaption>
                        <?php echo $caption ?>
                    </figcaption>
                    <?php endif; ?>
                <?php endif; ?>

                </div>
                <!-- /.content -->

            </aside>

        <?php endwhile; ?>
        <?php // end each column ?>


        </div>
        <!-- /.row -->

    </div>
    <!-- /.container_class -->
    </div>

    <?php endwhile; ?>

</div>
