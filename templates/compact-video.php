<?php

/**
* Compact video
*/

$summary = isset($template_args['summary']) ? $template_args['summary'] : false;
$is_serie = ouisurf_is_serie( $post );
$episode_number = get_field('episode_number', $post->ID);

?>

<article data-href="<?php the_permalink(); ?>" <?php post_class('post-compact--video'); ?>>
    <div class="content">

        <figure class="swiper-lazy bg-cover b-lazy box--sixteen-nine" data-src="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'large' ); ?>">
            <div class="content">
                <a href="<?php the_permalink(); ?>" class="btn-play--outlined small bottom-left"><i class="ion-ios-play"></i></a>
                <header>
                    <time class="video--duration">
                        <?php echo get_field('duration') ?>
                    </time>
                </header>
            </div>
            <img src="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'pixel' ); ?>" alt="" class="preload-pixel" />
        </figure>

        <header class="entry-meta">
            <h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <?php if ($is_serie): ?>
                <h6 class="entry-category">
                    <?php the_terms( get_the_ID(), 'series', null, ' - ' ) ?>
                    <?php if ($episode_number): ?>
                        - <?php _e('Épisode', 'ouisurf') ?> <?php echo $episode_number ?>
                    <?php endif; ?>
                </h6>
            <?php else: ?>
                <h6 class="entry-category"><?php the_terms( get_the_ID(), 'videos', null, ' - ' ) ?></h6>
            <?php endif; ?>
        </header>

        <?php if ($summary): ?>
            <div class="entry-summary">
                <?php the_excerpt(); ?>
            </div>
        <?php endif; ?>

    </div>
</article>
