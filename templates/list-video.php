<?php

/**
 * Post list format : default
*/

?>

<article <?php post_class('post-list--item'); ?>>
	<a href="<?php the_permalink(); ?>">
		<div class="row">
			<figure class="col-xs-5 no-padding--right">
				<img src="<?php echo ouisurf_post_get_post_thumbnail_url($post->ID, "video") ?>" alt="" class="img-responsive">
				<figcaption>
					<i class="ion-ios-play post-icon"></i>
					<time class="video--duration">
						<?php echo get_field('duration') ?>
					</time>
				</figcaption>
			</figure>
		  <header class="col-xs-7">
		    <h2 class="h5 entry-title"><?php the_title(); ?></h2>
				<time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
		  </header>
		</div>
	</a>
</article>
