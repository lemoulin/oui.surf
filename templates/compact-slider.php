<?php

/**
* Featured post
*/

$url                  = null;
$slider_url           = get_field('url');
$attached_post        = get_field('post');
$fullscreen           = get_field('fullscreen');
$link_label           = get_field('link_label');
$logo_image           = get_field('logo_image');
$logo_centered        = get_field('logo_centered');
$background_video     = get_field('background_video');
$video_id_vimeo       = get_field('video_id_vimeo');
$video_id_youtube     = get_field('video_id_youtube');
$video_button_label   = get_field('video_button_label');

if ($slider_url) {
    $url = $slider_url;
} else if($attached_post) {
    $url = get_the_permalink($attached_post);
}

// classname
if ($fullscreen) {
    $classname = 'post-slider is-fullscreen';
} else {
    $classname = 'post-slider';
}

?>

<article <?php post_class($classname); ?>>
    <header class="post-header">
        <div class="row">
            <div class="col-12">
                <?php if ( $logo_image ): ?>
                <figure>
                    <?php if ($url): ?>
                    <a href="<?php echo $url ?>">
                    <?php endif; ?>
                        <img src="<?= $logo_image ?>" alt="<?php the_title(); ?>" class="img-responsive <?php if ( $logo_centered ): ?>is-centered<?php endif; ?>" />
                    <?php if ($url): ?>
                    </a>
                    <?php endif; ?>
                </figure>
                <?php else: ?>    
                <?php endif; ?>
                <h2 class="h3"><?php if ($url): ?><a href="<?php echo $url ?>"><?php endif; ?><?php the_title(); ?><?php if ($url): ?></a><?php endif; ?></h2>
                <nav>
                    
                    <?php if ( $video_id_vimeo && $video_button_label ): ?>
                    <a href="#video-vimeo-<?php echo $video_id_vimeo ?>" data-video-player-trigger data-video-player-hide-navigation="true" class="btn btn-outlined-inverted inline-block--margin-right">
                        <?php echo $video_button_label ?>
                    </a>    
                    <?php endif; ?>

                    <?php if ( $video_id_youtube && $video_button_label ): ?>
                    <a href="#video-youtube-<?php echo $video_id_youtube ?>" data-video-player-trigger data-video-player-hide-navigation="true" class="btn btn-outlined-inverted inline-block--margin-right">
                        <?php echo $video_button_label ?>
                    </a>    
                    <?php endif; ?>
                    
                    <?php if ($url): ?>
                    <a href="<?php echo $url ?>" class="btn btn-outlined-inverted inline-block--margin-right">
                        <?php if ( $link_label ): ?>
                            <?php echo $link_label ?>
                        <?php else: ?>    
                            <?php _e( "En savoir plus", "ouisurf" ) ?>
                        <?php endif; ?>
                    </a>
                    <?php endif; ?>

                    <?php if (!$url && $link_label): ?>
                        <span class="inline-block inline-block--margin-right"><?php echo $link_label ?></span>
                    <?php endif; ?>

                    

                </nav>
            </div>
        </div>
    </header>
    
    <!-- embeded player -->
    <?php if ($video_id_vimeo): ?>
        <div id="video-vimeo-<?php echo $video_id_vimeo; ?>" data-video-id="<?php echo $video_id_vimeo; ?>" class="video-player--embed">
            <div class="content">
                <a href="#video-<?php echo $video_id_vimeo ?>" class="btn-close-video"><i class="ion-android-close"></i></a>
                <iframe data-service="vimeo" id="video-<?php echo $video_id_vimeo ?>" src="https://player.vimeo.com/video/<?php echo $video_id_vimeo; ?>?color=ffffff&title=0&byline=0&portrait=0&api=1&id=video-<?php echo $video_id_vimeo ?>" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($video_id_youtube): ?>
        <div id="video-youtube-<?php echo $video_id_youtube; ?>" data-video-id="<?php echo $video_id_youtube; ?>" class="video-player--embed">
            <div class="content">
                <a href="#video-<?php echo $video_id_youtube ?>" class="btn-close-video"><i class="ion-android-close"></i></a>
                <iframe data-service="youtube" src="https://www.youtube.com/embed/<?php echo $video_id_youtube; ?>?api=1" width="100%" height="500" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
        </div>
    <?php endif; ?>

    <div class="bg-filled bg-cover b-lazy is-header" data-src="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'largest' ); ?>" <?php if ($background_video): ?>data-video-bg="<?php echo $background_video ?>"<?php endif; ?>>
        <img src="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'pixel' ); ?>" alt="" class="preload-pixel" />
    </div>
</article>
