<?php


// get current category term ID
$taxonomy_id = get_queried_object_id();
$rows_query_selector = "series_$taxonomy_id";

// get recents video excluding current serie category
$latest_post_args = array(
  'posts_per_page' => 20,
  'post_type' => 'post',
  'orderby' => 'rand',
  'tax_query'	=> array(
    'relation' => 'AND',
    // not in current series taxo
    array(
      'taxonomy'  => 'series',
      'field'     => 'id',
      'terms'     => array($taxonomy_id),
      'operator'  => 'NOT IN'
    ),
    // in section = series
    array(
      'taxonomy'  => 'ouisurf_section',
      'field'     => 'slug',
      'terms'     => 'series'
    ),
   )
);

$latest_posts = new WP_Query( $latest_post_args );

?>

<section class="serie" data-controller="Serie">

  <?php get_template_part('templates/content-type/series/serie-header'); ?>

  <?php hm_get_template_part('templates/partials/photos-album', array('rows_query_selector' => $rows_query_selector)); ?>

  <!-- Other random series posts -->
  <div class="container-fluid section--sep">
    <div class="row">
      <h4 class="col-md-offset-1 col-sm-11 col-xs-12 no-padding--min-sm"><?php _e('Très cool aussi', 'ouisurf') ?></h4>
    </div>
  </div>

  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-11 col-sm-offset-1 col-xs-12 no-padding--min-sm">

        <!-- posts -->
        <?php if ($latest_posts->have_posts()) : ?>
          <div data-slider-default data-slides-gutter-before="0" data-slides-per-view="3.25" data-slides-per-view-mobile="1.085" class="swiper-container serie--posts-slider">
            <div class="swiper-wrapper">
              <?php while ($latest_posts->have_posts()) : $latest_posts->the_post(); ?>

                <div class="swiper-slide">
                  <?php get_template_part('templates/compact-video'); ?>
                </div>
              <?php endwhile; ?>
            </div>
          </div>
        <?php endif; ?>
        <!-- end posts  -->

      </div>
    </div>

    <!-- all series -->
    <?php get_template_part('templates/content-type/series/all-series'); ?>

  </div>

  <?php wp_reset_postdata(); ?>



</section>
