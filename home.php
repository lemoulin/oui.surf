<?php

use Roots\Sage\Titles;

$paged = get_query_var('paged', 1);

$posts_per_page = get_option( 'posts_per_page' );

// remove sticky posts from main query
$posts = query_posts(array(
    'post__not_in' => get_option( 'sticky_posts' ),
    'paged' => $paged,
    'meta_query' => array(
        'relation'     => 'OR',
        array(
            'key'      => 'hide_from_home',
            'compare'  => 'NOT EXISTS'
		),
        array(
            'key'      => 'hide_from_home',
            'value'    => '0',
            'compare'  => '='
		)
	)
));

// Featured posts
$recent_posts = Array();
isset($posts[0]) ? $recent_posts[0] = $posts[0] : null;
isset($posts[1]) ? $recent_posts[1] = $posts[1] : null;
isset($posts[2]) ? $recent_posts[2] = $posts[2] : null;
isset($posts[3]) ? $recent_posts[3] = $posts[3] : null;

// most recent sliders post
$sliders_posts = new WP_Query(array(
    'posts_per_page' => 1,
    'post_type' => 'sliders'
));

$sliders_posts_is_fullscreen = get_field('fullscreen', $sliders_posts->posts[0]);
// echo $sliders_posts_is_fullscreen;

// recents sticky posts
$sticky_posts_args = array(
	'date_query' => array(
		array(
			'column' => 'post_date_gmt',
			'after' => '1 mounth ago',
		)
	),
    'post__in'  => get_option( 'sticky_posts' ),
    'posts_per_page' => 10,
    'post_type' => 'post'
);
$home_sticky_posts = new WP_Query( $sticky_posts_args );


?>

<section class="archive-default" data-controller="Home">

    <header class="archive-header <?php if ($sliders_posts->post_count): ?>with-slider-post<?php endif; ?> <?php if ($sliders_posts_is_fullscreen): ?>with-slider-post-fullscren<?php endif; ?>">
        <div class="content">

            <?php if ($sliders_posts->post_count > 0): ?>
                <?php while ($sliders_posts->have_posts()) : $sliders_posts->the_post(); ?>
                    <?php get_template_part('templates/compact-slider'); ?>
                <?php endwhile; ?>
                <?php wp_reset_postdata() ?>
            <?php endif; ?>

            <?php if (isset($recent_posts[0])): ?>
                <?php $post = $recent_posts[0] ?>
                <?php get_template_part('templates/featured', ouisurf_post_get_section($post)); ?>
            <?php endif; ?>

            <?php if (isset($recent_posts[1])): ?>
                <?php $post = $recent_posts[1] ?>
                <?php get_template_part('templates/featured', ouisurf_post_get_section($post)); ?>
            <?php endif; ?>

        </div>
    </header>

    <?php if ($home_sticky_posts->post_count >= 3): ?>
    <div class="archive--sticky-posts container-fluid container-fluid--max-width-xl">
        <div class="row">

            <header class="col-sm-12 section--sep">
                <h4 class="no-margin--top"><?php _e( "Choix de l'équipe", "ouisurf" ) ?></h4>
				        <nav id="slider-controls-sticky" class="section--sep--menu">
  		            <a href="#" class="slider--arrow--prev"><i class="ion-ios-arrow-left"></i></a>
  		            <a href="#" class="slider--arrow--next"><i class="ion-ios-arrow-right"></i></a>
		            </nav>
            </header>

            <div class="col-sm-12 no-padding--left">

                <div data-slider-default data-slides-per-view="5.25" data-slider-arrows-container="#slider-controls-sticky" class="swiper-container slider--default">
                    <div class="swiper-wrapper">

                      <?php while ($home_sticky_posts->have_posts()) : $home_sticky_posts->the_post(); ?>
                      <?php if ($post->ID != $recent_posts[0]->ID && $post->ID != $recent_posts[1]->ID && $post->ID != $recent_posts[2]->ID && $post->ID != $recent_posts[3]->ID): ?>
                          <div class="swiper-slide">
                              <?php get_template_part('templates/small'); ?>
                          </div>
                      <?php endif; ?>
                      <?php endwhile; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <?php endif; ?>

    <?php // first 2 posts ?>
    <div class="container-fluid container-fluid--max-width-xl">

        <div class="row">

            <div class="archive-posts">

                <?php if (isset($recent_posts[2])): ?>
                    <?php $post = $recent_posts[2]; global $post; ?>
                    <?php get_template_part('templates/compact', ouisurf_post_get_section($post)); ?>
                <?php endif; ?>

                <?php if (isset($recent_posts[3])): ?>
                    <?php $post = $recent_posts[3]; global $post; ?>
                    <?php get_template_part('templates/compact', ouisurf_post_get_section($post)); ?>
                <?php endif; ?>

            </div>

            <aside class="archive-sidebar">
                <?php dynamic_sidebar('sidebar-homepage'); ?>
            </aside>

        </div>



    </div>

    <?php
    /**
    * Recents videos
    */
    if ($paged == 0):

    $recent_videos = new WP_Query(array(
        'posts_per_page' => 6,
        'taxonomy' => 'ouisurf_section',
        'field' => 'slug',
        'term' => 'videos'
    ));

    ?>
    <?php if ($recent_videos->post_count > 1): ?>
    <div class="archives--hightlighted-posts">
        <div class="container-fluid container-fluid--max-width-xl">

            <header class="row">
                <h3 class="col-xs-12 h4"><?php _e( "Vidéos récentes", "ouisurf" ) ?></h3>
            </header>

            <section data-slider-default data-slides-effect="fade" data-slides-per-view-mobile="1"  -data-slider-control=".hightlighted-posts--nav-slider" class="hightlighted-posts--content-slider swiper-container slider--default row">
                <div class="swiper-wrapper col-xs-12">
                <?php while ($recent_videos->have_posts()) : $recent_videos->the_post(); ?>
                    <div class="swiper-slide">
                        <?php get_template_part('templates/compact-video-slide') ?>
                    </div>
                <?php endwhile; ?>
                </div>
            </section>

            <nav data-slider-default data-slides-per-view="6" data-slides-per-view-mobile="2.25" data-slides-gutter-before="0" -data-slides-gutter-after="null" data-slider-nav data-slider-nav-control=".hightlighted-posts--content-slider" class="hightlighted-posts--nav-slider swiper-container slider--default row">
                <div class="swiper-wrapper">
                    <?php while ($recent_videos->have_posts()) : $recent_videos->the_post(); ?>
                    <div class="swiper-slide">
                        <a href="#" class="hightlighted-posts--nav-slide">
                          <figure class="no-padding b-lazy bg-cover" data-src="<?php echo ouisurf_post_get_post_thumbnail_url( get_the_ID(), 'medium' ); ?>">
                              <?php if (get_field("vimeo_video_id", $post->ID) || get_field("youtube_video_id", $post->ID)): ?>
                              <i class="play-icon ion-ios-play"></i>
                              <?php endif; ?>
                              <?php if (get_field("duration", $post->ID)): ?>
                                  <time class="video--duration">
                                      <?php the_field('duration') ?>
                                  </time>
                              <?php endif; ?>
                          </figure>
                          <aside class="no-padding--left">
                              <h3 class="h5"><?php the_title() ?></h3>
                              <time class="updated hidden-xs" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
                          </aside>
                        </a>
                    </div>
                    <?php endwhile; ?>
                </div>
            </nav>
        </div>
    </div>
    <?php endif; ?>
    <?php endif; ?>

    <?php // the rest of the posts  ?>
    <div class="container-fluid container-fluid--max-width-xl">
        <div class="row">

            <div class="archive-posts">
                <?php get_template_part('templates/partials', 'no-results'); ?>

                <?php $i = 1; while (have_posts()) : the_post(); ?>

                    <?php if ($post->ID != $recent_posts[0]->ID && $post->ID != $recent_posts[1]->ID && $post->ID != $recent_posts[2]->ID && $post->ID != $recent_posts[3]->ID): ?>
                    <?php get_template_part('templates/compact', ouisurf_post_get_section($post)); ?>
                    <?php endif; ?>

                    <!-- ads -->
                    <?php if ($i == ($posts_per_page / 2)): ?>
                    <div class="archives-ads">
                        <?php dynamic_sidebar( 'archives-ads' ); ?>
                    </div>
                    <?php endif ?>

                <?php $i++; endwhile; ?>

                <?php
                    the_posts_navigation(
                        array(
                            "prev_text" => sprintf( "%s <i class='ion-ios-arrow-right'></i>", __("Page suivante", "ouisurf") ),
                            "next_text" => sprintf( "<i class='ion-ios-arrow-left'></i> %s", __("Page précédente", "ouisurf") )
                        )
                    )
                ?>

            </div>

            <aside class="archive-sidebar" data-sticky>
                <?php get_template_part("templates/partials/sidebar-sticky-posts") ?>
            </aside>

        </div>
    </div>
    <!-- /.container  -->

</section>
